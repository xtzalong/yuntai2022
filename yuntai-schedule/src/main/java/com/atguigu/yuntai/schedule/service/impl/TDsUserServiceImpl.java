package com.atguigu.yuntai.schedule.service.impl;

import com.atguigu.yuntai.schedule.entity.TDsUser;
import com.atguigu.yuntai.schedule.mapper.TDsUserMapper;
import com.atguigu.yuntai.schedule.service.TDsUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  用户实现类
 * </p>
 */
@Service
public class TDsUserServiceImpl extends ServiceImpl<TDsUserMapper, TDsUser> implements TDsUserService {
}
