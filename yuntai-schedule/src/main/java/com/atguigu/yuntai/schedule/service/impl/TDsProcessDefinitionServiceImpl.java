package com.atguigu.yuntai.schedule.service.impl;

import com.atguigu.yuntai.schedule.entity.TDsProcessDefinition;
import com.atguigu.yuntai.schedule.mapper.TDsProcessDefinitionMapper;
import com.atguigu.yuntai.schedule.service.TDsProcessDefinitionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TDsProcessDefinitionServiceImpl extends ServiceImpl<TDsProcessDefinitionMapper, TDsProcessDefinition> implements TDsProcessDefinitionService {

    public List<Long> getReleastCodeList(){
        List<Long> releastCodeList=new ArrayList<>();
        List<String> taskCodeList = this.baseMapper.getTaskCodes();
        if(CollectionUtils.isNotEmpty(taskCodeList)){
            for(String taskCode : taskCodeList){
                String[] idArray = StringUtils.split(taskCode, "[|,|]");
                List<Long> codeList = Arrays.stream(idArray)
                        .map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                releastCodeList.addAll(codeList);

            }
        }
        return releastCodeList;
    }
}
