package com.atguigu.yuntai.schedule.bean;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 */
@Data
@AllArgsConstructor
@ApiModel(description = "表查询实体")
public class DSTaskAvgTimeInstance {
    private Long task_code;
    private String task_type;
    private Long avgTime;
}
