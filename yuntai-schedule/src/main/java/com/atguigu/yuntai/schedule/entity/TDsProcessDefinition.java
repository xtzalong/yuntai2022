package com.atguigu.yuntai.schedule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * 流程定义实体类
 */
@Data
@ApiModel(description = "流程定义实体类")
@TableName("t_ds_process_definition")
@AllArgsConstructor
@NoArgsConstructor
public class TDsProcessDefinition {
    /**
     * Id
     */
    @ApiModelProperty(value = "Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 编码
     */
    @ApiModelProperty(value = "编码")
    @TableField("code")
    private Long code;
    /**
     * 任务定义名称
     */
    @ApiModelProperty(value = "任务定义名称")
    @TableField("name")
    private String name;
    /**
     * 版本
     */
    @ApiModelProperty(value = "版本")
    @TableField("version")
    private Integer version;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    @TableField("description")
    private String description;
    /**
     * 项目编码
     */
    @ApiModelProperty(value = "项目编码")
    @TableField("project_code")
    private Long projectCode;
    /**
     * 释放状态
     */
    @ApiModelProperty(value = "释放状态")
    @TableField("release_state")
    private Byte releaseState;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    /**
     * 全局参数
     */
    @ApiModelProperty(value = "全局参数")
    @TableField("global_params")
    private String globalParams;
    /**
     * 标志
     */
    @ApiModelProperty(value = "标志")
    @TableField("flag")
    private Byte flag;
    /**
     * 位置
     */
    @ApiModelProperty(value = "位置")
    @TableField("locations")
    private String locations;
    /**
     * 报警组id
     */
    @ApiModelProperty(value = "报警组id")
    @TableField("warning_group_id")
    private Integer warningGroupId;
    /**
     * 超时
     */
    @ApiModelProperty(value = "超时")
    @TableField("timeout")
    private Integer timeout;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    @TableField("tenant_id")
    private Integer tenantId;
    /**
     * 执行类型
     */
    @ApiModelProperty(value = "执行类型")
    @TableField("execution_type")
    private Byte executionType;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Timestamp createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    private Timestamp updateTime;

}
