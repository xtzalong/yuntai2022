package com.atguigu.yuntai.schedule.service;

import com.atguigu.yuntai.schedule.entity.TDsUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  用户服务接口
 * </p>
 *
 */
public interface TDsUserService extends IService<TDsUser> {

}
