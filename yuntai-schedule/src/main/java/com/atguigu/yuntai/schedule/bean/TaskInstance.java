package com.atguigu.yuntai.schedule.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_ds_task_instance")
public class TaskInstance {

    /**
     * 编号
     */
    private int id;

    /**
     *
     */
    private BigInteger code;

    /**
     *名称
     */
    private String name;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 节点类型
     */
    private String taskType;

    /**
     * 状态
     */
    private int state;

    /**
     * 提交时间
     */
    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date submitTime;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date startTime;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date endTime;

    /**
     * 重试次数
     */
    private int retryTimes;

    /**
     * 期望时间
     */
    private String expectTime;


    private int taskInstanceId;

    private String userName;

    private int userId;

    private String taskParams;

    private String tableName;

    private int failRetryTimes;

    private int failRetryInterval;

    private int delayTime;

    private int timeoutFlag;

    private int timeoutNotifyStrategy;

    private int timeout;

    private String sql;


}
