package com.atguigu.yuntai.schedule.mapper;


import com.atguigu.yuntai.schedule.entity.TDsTaskInstance;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 *任务实例Mapper
 */
@DS("mysql-ds")
public interface TDsTaskInstanceMapper extends BaseMapper<TDsTaskInstance> {


    @Select("SELECT COUNT(*) FROM t_ds_task_instance where NAME =#{tableName}  AND DATE_FORMAT(start_time,'%Y-%m-%d')=#{busiDate} AND state=6 ")
    public Long countErrorTaskByTableName(@Param("busiDate")String busiDate,@Param("tableName") String tableName);

    @Select("select task_json from t_ds_task_instance ")
    public List<String> getTaskJsonFromDS();

    /**
     * 查询前三天（不包括今天）的历史任务数
     *
     * @param task_code
     * @return
     */
    @Select("SELECT COUNT(id) FROM t_ds_task_instance WHERE (TO_DAYS(NOW())-TO_DAYS(end_time))>=1 AND " +
            "(TO_DAYS(NOW())-TO_DAYS(end_time))<=3  AND state=7  AND task_code=#{task_code}")
    public int getTaskLastThreeDaysCount(Long task_code);


}
