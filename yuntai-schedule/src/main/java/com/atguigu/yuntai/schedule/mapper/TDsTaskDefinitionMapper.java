package com.atguigu.yuntai.schedule.mapper;


import com.atguigu.yuntai.schedule.entity.TDsTaskDefinition;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 任务定义Mapper
 */
@Mapper
@DS("mysql-ds")
public interface TDsTaskDefinitionMapper extends BaseMapper<TDsTaskDefinition> {

    /**
     * 查询所有sql类型的taskparam
     * @param taskCodes
     * @return
     */
    @Select("<script> " +
            "select task_params from t_ds_task_definition where task_type='SQL' and code in" +
            "<foreach collection='list' item = 'taskCodes' index='index' open='(' close=')' separator=','>" +
            "#{taskCodes}</foreach>" +
            "</script> ")
    public List<String> getTaskDefinitionSQL(List<Long> taskCodes );

    /**
     * 根据
     * @param taskCodes
     * @param tableName
     * @return
     */
    @Select("<script> " +
            "select task_params from t_ds_task_definition where task_type='SQL' and name=#{tableName} and code in" +
            "<foreach collection='taskCodes' item = 'taskCode' index='index' open='(' close=')' separator=','>" +
            "#{taskCode}</foreach>" +
            "</script> ")
    public String getTaskDefinitionSQLByName(@Param("taskCodes") List<Long> taskCodes, @Param("tableName") String  tableName );

    @Select("<script> " +
            "select * from t_ds_task_definition where task_type='SQL' and name=#{tableName} and code in" +
            "<foreach collection='taskCodes' item = 'taskCode' index='index' open='(' close=')' separator=','>" +
            "#{taskCode}</foreach>" +
            "</script> ")
    public TDsTaskDefinition getTaskDefinitionByName(@Param("taskCodes") List<Long> taskCodes, @Param("tableName") String  tableName );


    @Select("<script> " +
            "select task_params from t_ds_task_definition where task_type='SHELL' and code in" +
            "<foreach collection='list' item = 'taskCode' index='index' open='(' close=')' separator=','>" +
            "#{taskCode}</foreach>" +
            "</script> ")
    public List<String> getTaskDefinitionSHELL(List<Long> taskCodes );

    @Select("<script> " +
            "select task_params from t_ds_task_definition where task_type='SHELL' and name=#{tableName} and code in" +
            "<foreach collection='taskCodes' item = 'taskCodes' index='index' open='(' close=')' separator=','>" +
            "#{taskCodes}</foreach>" +
            "</script> ")
    public  String  getTaskDefinitionSHELLByName(@Param("taskCodes") List<Long> taskCodes, @Param("tableName") String  tableName );

}
