package com.atguigu.yuntai.schedule.mapper;

import com.atguigu.yuntai.schedule.bean.DSTaskAvgTimeInstance;
import com.atguigu.yuntai.schedule.bean.DSTaskTimeInstance;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 */
@DS("mysql-ds")
public interface DSTimeTaskGovernMapper {

    /**
     * 查询最前三天数据（不包含当天数据） 并且是成功的任务
     *
     * @return
     */
    @Select("SELECT  dti.task_code,dti.task_type,AVG(TIMESTAMPDIFF(SECOND,start_time,end_time))FROM t_ds_task_instance dti" +
            "     LEFT JOIN (SELECT *FROM govern_alert_notified WHERE TO_DAYS(TIME) = TO_DAYS(NOW())) gan ON dti.task_code=gan.task_code" +
            "    WHERE (TO_DAYS(NOW())-TO_DAYS(end_time))>=1 AND (TO_DAYS(NOW())-TO_DAYS(end_time))<=3  AND state=7 " +
            "     AND gan.task_code IS NULL" +
            "    GROUP BY task_code,task_type")
    public List<DSTaskAvgTimeInstance> getSucessAvgTimeTaskLastThreeDays();

    /**
     * 查询当天的任务
     *
     * @return
     */
    @Select("SELECT dti.`name`,dti.task_code,dti.task_type,dti.submit_time,dti.start_time,dti.end_time," +
            "      TIMESTAMPDIFF(SECOND,dti.start_time,dti.end_time),dti.state,dpd.user_id,du.phone,du.email " +
            "      FROM t_ds_task_instance dti INNER JOIN  t_ds_process_instance dpi ON dti.process_instance_id=dpi.id " +
            "      INNER JOIN  t_ds_process_definition dpd ON dpi.process_definition_code=dpd.code " +
            "      INNER JOIN t_ds_user du ON dpd.user_id=du.id " +
            "      LEFT JOIN (SELECT *FROM govern_alert_notified WHERE TO_DAYS(TIME) = TO_DAYS(NOW())) gan ON dti.task_code=gan.task_code " +
            "      WHERE TO_DAYS(dti.end_time) = TO_DAYS(NOW()) AND gan.task_code IS NULL")
    public List<DSTaskTimeInstance> getTodayTaskTime();

    /**
     * 将已预警的数据插入的mysql表中，定时器查询数据库时进行过滤
     */
    @Insert("INSERT INTO govern_alert_notified(NAME,task_code,task_type,TIME) VALUES(#{name},#{task_code},#{task_type},#{time})")
    public void saveGovernAlertNotified(String name, Long task_code, String task_type, Timestamp time);

}
