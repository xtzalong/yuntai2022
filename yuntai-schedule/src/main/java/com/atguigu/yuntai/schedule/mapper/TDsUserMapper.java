package com.atguigu.yuntai.schedule.mapper;

import com.atguigu.yuntai.schedule.entity.TDsUser;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  用户 Mapper
 * </p>
 *
 */
@DS("mysql-ds")
public interface TDsUserMapper extends BaseMapper<TDsUser> {

}
