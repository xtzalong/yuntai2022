package com.atguigu.yuntai.schedule.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.yuntai.schedule.bean.QTaskInstance;
import com.atguigu.yuntai.schedule.bean.TaskInstance;
import com.atguigu.yuntai.schedule.entity.TDsTaskDefinition;
import com.atguigu.yuntai.schedule.mapper.ScheduleMapper;
import com.atguigu.yuntai.schedule.mapper.TDsProcessDefinitionMapper;
import com.atguigu.yuntai.schedule.mapper.TDsTaskDefinitionMapper;
import com.atguigu.yuntai.schedule.service.ScheduleService;
import com.atguigu.yuntai.schedule.service.TDsProcessDefinitionService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


@Service
public class ScheduleServiceImpl  extends ServiceImpl<ScheduleMapper, TaskInstance> implements ScheduleService {

    @Autowired
    private ScheduleMapper scheduleMapper;

    @Autowired
    private TDsProcessDefinitionService tDsProcessDefinitionService;

    @Override
    public IPage<TaskInstance> getScheduleList(Page<TaskInstance> pageParam, QTaskInstance qvTaskInstance) {
        long taskCode = 0L;
        List<Long> taskCodes = tDsProcessDefinitionService.getReleastCodeList();
        qvTaskInstance.setTaskCodes(taskCodes);
//        TaskInstance vTaskInstance = scheduleMapper.getScheduleList(pageParam,qvTaskInstance);
        return this.baseMapper.getScheduleList(pageParam,qvTaskInstance);
    }

    @Override
    public TaskInstance getSchedelDetailById(BigInteger code) {
        TaskInstance schedelDetailById = scheduleMapper.getSchedelDetailById(code);
        if (schedelDetailById != null && StringUtils.isNotEmpty(schedelDetailById.getTaskParams())) {
            String taskParams = schedelDetailById.getTaskParams();
            JSONObject jsonObject = JSONObject.parseObject(taskParams);
            String sql = jsonObject.getString("sql");
            if(StringUtils.isNotEmpty(sql)){
                int tIdx = sql.indexOf("table") + 6;
                int nIdx = sql.indexOf("\n", tIdx);
                int spIdx = sql.indexOf(" ", tIdx);
                int min = Math.min(nIdx, spIdx);
                String tableName = sql.substring(tIdx, min);
                schedelDetailById.setTableName(tableName);
            }
        }
        return schedelDetailById;
    }

    @Override
    public TaskInstance getSchedelDetailByName(String name) {
        TaskInstance schedelDetailByName = scheduleMapper.getSchedelDetailByName(name);
        if (schedelDetailByName != null && StringUtils.isNotEmpty(schedelDetailByName.getTaskParams())) {
            String taskParams = schedelDetailByName.getTaskParams();
            JSONObject jsonObject = JSONObject.parseObject(taskParams);
            String sql = jsonObject.getString("sql").replaceAll("\\\\n", "\r\n");
//            String[] array = sql.split("\\(");
//            StringBuilder sb = new StringBuilder();
//            for (String str : array){
//              sb.append(str).append("\n");
//            }
            schedelDetailByName.setSql(sql);

        }
        return schedelDetailByName;
    }
}
