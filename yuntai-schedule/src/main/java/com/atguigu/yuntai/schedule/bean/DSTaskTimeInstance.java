package com.atguigu.yuntai.schedule.bean;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

/**
 *任务实例实体
 */
@Data
@AllArgsConstructor
@ApiModel(description = "任务实例实体")
public class DSTaskTimeInstance {
    private String name;
    private Long task_code;
    private String task_type;
    private Timestamp submit_time;
    private Timestamp start_time;
    private Timestamp end_time;
    private Long time_second; //任务耗时秒数  end_time-start_time
    private int state; //任务实例状态：0 提交成功,1 正在运行,2 准备暂停,3 暂停,4 准备停止,5 停止,6 失败,7 成功,8 需要容错,9 kill,10 等待线程,11 等待依赖完成
    private long uid;
    private String phone;
    private String email;
}
