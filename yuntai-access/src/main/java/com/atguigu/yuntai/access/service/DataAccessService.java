package com.atguigu.yuntai.access.service;

import com.atguigu.yuntai.access.bean.ConnectorBean;
import com.atguigu.yuntai.common.util.Result;

import java.util.List;

public interface DataAccessService {
    //新增Connector Post
    public Result registerConnector(ConnectorBean connector);

    //删除Connector
    public Result deleteConnector(String name);

    //暂停Connector
    public Result pauseConnector(String name);

    //恢复Connector
    public Result resumeConnector(String name);

    //查询所有Connector
    public List<ConnectorBean> getConnectorList(String category);

    //查询Connector详情
    public ConnectorBean getStatus(String name);

    //更新Connector配置 Post
    public Result updateConnector(ConnectorBean connector);

    //获取Connector所有Task

    //获取指定Task状态

    //重启Connector

    //重启Task

    //获取Connector的Topic

    //清空指定Topic
}
