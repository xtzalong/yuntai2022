package com.atguigu.yuntai.common.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DtCount {
    String dt;
    Long ct;
}
