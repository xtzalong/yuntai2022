package com.atguigu.yuntai.report.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: gmall
 * @description:表头信息
 */

/**
 * 表头信息
 */
@Data
@ApiModel(description = "表头信息实体")
public class TableHeader implements Serializable {

    @ApiModelProperty(value = "表信息")
    private String label;

    @ApiModelProperty(value = "状态")
    private String property;

    public TableHeader(String label, String property) {
        this.property = property;
        this.label = label;
    }
}
