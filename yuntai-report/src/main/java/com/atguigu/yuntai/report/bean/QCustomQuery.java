package com.atguigu.yuntai.report.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询类
 */
@Data
@ApiModel(description = "sql查询实体")
public class QCustomQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "查询sql")
    private String sql;

    @ApiModelProperty(value = "数据源类型")
    private String typeMetaData;


}
