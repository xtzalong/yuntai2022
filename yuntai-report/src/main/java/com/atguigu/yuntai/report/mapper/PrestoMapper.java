package com.atguigu.yuntai.report.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * presto操纵mapper
 */
@DS("presto")
public interface PrestoMapper {

    @Select("${sql}")
    List<LinkedHashMap<String, Object>> findPrestoList(@Param("sql") String sql);
}
