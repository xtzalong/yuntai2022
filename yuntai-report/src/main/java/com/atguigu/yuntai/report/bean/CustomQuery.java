package com.atguigu.yuntai.report.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * sql查询后返回实体
 */
@Data
@ApiModel(description = "sql返回实体")
public class CustomQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回map")
    private List<LinkedHashMap<String, Object>> queryMap;

    @ApiModelProperty(value = "返回头信息list")
    private List<TableHeader> tableHeaderList;

    private List<Map<String, Object >> query;
}
