package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 表名规范评分器
 */
@Component("IS_TABLE_NAME_SPEC")
@Slf4j
public class TableNameSpecJudger implements  Judger{

    //表名规范
    public   final String OPTION_CODE="IS_TABLE_NAME_SPEC";
    //规范
    public   final String OPTION_TYPE="SPEC";

    /**
     * 根据表名是否规范进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        //前缀
        String[] tablePrefix = {"ods_", "dwd_", "dws_", "dim_", "ads_"};
        Boolean ifStartWith=false;
        for (String prefix : tablePrefix) {
              ifStartWith = tableInfo.getTableName().startsWith(prefix);
              if(ifStartWith){
                break;
              }
        }


        if (ifStartWith){
            governanceDetail.setGovernanceContent("符合规范");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else {
            governanceDetail.setGovernanceContent("表名不符合规范" );
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }





        return governanceDetail;
    }
}
