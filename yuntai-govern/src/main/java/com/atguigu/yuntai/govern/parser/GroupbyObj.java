package com.atguigu.yuntai.govern.parser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.hadoop.hive.ql.parse.ASTNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 解析语法树，根据group by进行解析
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupbyObj {

    ASTNode joinASTNode;

    List<TableObj> tableList = new ArrayList();

    public GroupbyObj(ASTNode joinASTNode) {
        handleGroupBy(joinASTNode);
    }

    private void handleGroupBy(ASTNode joinASTNode) {
        for (int i = 0; i < joinASTNode.getChildCount(); i++) {
            ASTNode node = (ASTNode) joinASTNode.getChild(i);
            if ("TOK_TABREF".equals(node.getText())) {
                tableList.add(new TableObj(node));
            } else if ("TOK_JOIN".equals(node.getText()) || "TOK_LEFTOUTERJOIN".equals(node.getText()) || "TOK_LEFTOUTERJOIN".equals(node.getText())) {
                handleGroupBy(node);
            }

        }
    }

}
