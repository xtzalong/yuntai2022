package com.atguigu.yuntai.govern.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 表查询条件
 */
@Data
@NoArgsConstructor
@ApiModel(description = "表查询实体")
public class QTable {

    @ApiModelProperty(value = "库名")
    private String schemaName;

    @ApiModelProperty(value = "表名")
    private String tableName;

}
