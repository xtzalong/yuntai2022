package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.schedule.entity.TDsTaskDefinition;
import com.atguigu.yuntai.schedule.entity.TDsTaskInstance;
import com.atguigu.yuntai.schedule.service.TDsTaskDefinitionService;
import com.atguigu.yuntai.schedule.service.TDsTaskInstanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 预期时效检查评分器
 */
@Component("TIMELINESS_CHECK")
@Slf4j
public class TimelinessJudger implements  Judger{

    //预期实效检查
    public   final String OPTION_CODE="TIMELINESS_CHECK";
    //质量
    public   final String OPTION_TYPE="QUALITY";
    //超时8个小时以上 记0分
    public   final Integer MAX_TIMEOUT_MINUTES= 60*8;



    @Autowired
    TDsTaskDefinitionService tDsTaskDefinitionService;

    @Autowired
    TDsTaskInstanceService tDsTaskInstanceService;

    /**
     * 根据预期时效进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        TDsTaskDefinition taskDefinition  = tDsTaskDefinitionService.getReleastTaskDefinitionByName(tableInfo.getTableName());
        if(taskDefinition==null){
            governanceDetail.setGovernanceContent("未发现任务");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
            return governanceDetail;
        }
        Integer timeoutMinutes = taskDefinition.getTimeout();

        TDsTaskInstance taskInstance = tDsTaskInstanceService.getSuccessTaskByTableName(busiDate, tableInfo.getTableName());

        if (taskInstance==null){
            governanceDetail.setGovernanceContent("未发现任务执行结果");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
            return governanceDetail;
        }
        long startTimeMs = taskInstance.getStartTime().getTime();
        long endTimeMs = taskInstance.getEndTime().getTime();

        Long runMinutes = (endTimeMs - startTimeMs )/ 1000 / 60;


        if (timeoutMinutes>=runMinutes ){
            governanceDetail.setGovernanceContent("未超时");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
        }else if(taskInstance==null){
            governanceDetail.setGovernanceContent("未成功完成任务");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
        }else{
            // 分数= 1-  超时分钟数/超时分钟数上限
            Long  beyondTimeoutMinutes = runMinutes - timeoutMinutes ;
            BigDecimal deductScore = BigDecimal.valueOf(beyondTimeoutMinutes).divide(BigDecimal.valueOf(MAX_TIMEOUT_MINUTES), 2, RoundingMode.HALF_UP);
            BigDecimal score=BigDecimal.ONE.subtract(deductScore);
            score=score.max(BigDecimal.ZERO);

            governanceDetail.setGovernanceContent("超时分钟:"+beyondTimeoutMinutes+"分钟");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(score);
        }


        governanceDetail.setOptionCode(OPTION_CODE);
        governanceDetail.setOptionType(OPTION_TYPE);
        return governanceDetail;
    }
}
