package com.atguigu.yuntai.govern.parser;

import lombok.Data;
import org.antlr.runtime.tree.Tree;
import org.apache.hadoop.hive.ql.parse.ASTNode;

/**
 * 解析语法树，是否包含聚合函数
 */
@Data
public class FunctionObj {

    String functionName;

    Boolean isAggFunction = false;

    String fieldName;

    public FunctionObj(ASTNode node) {
        Tree functionNode = node.getChild(0);
        functionName = functionNode.getText();
        //判断是否聚合函数(包括炸开函数)
        if (functionNode.getText().equals("sum") ||
                functionNode.getText().equals("count") ||
                functionNode.getText().equals("avg") ||
                functionNode.getText().equals("max") ||
                functionNode.getText().equals("min") ||
                functionNode.getText().equals("explode") ||
                functionNode.getText().equals("collect_set") ||
                functionNode.getText().equals("collect_list")
        ) {
            isAggFunction = true;
        }
        //如果涉及字段
        if (node.getChildCount() > 1) {
            if (node.getChild(1).getText().equals("TOK_TABLE_OR_COL")) {
                fieldName = node.getChild(1).getChild(0).getText();
            }
        }

    }
}
