package com.atguigu.yuntai.govern.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.sql.Timestamp;

/**
 * 预警记录mapper
 */
@DS("mysql-yuntai-config")
public interface GovernConfigMapper {

    @Select("select `value` from govern_config where `key`=#{key}")
    public String getValueByKey(String key);

    /**
     * 预警记录
     * @param context
     * @param phone
     * @param time
     */
    @Insert("INSERT INTO govern_phone_record(context,phone,`time`) VALUE(#{context},#{phone},#{time})")
    public void saveGovernPhoneRecord(String context, String phone, Timestamp time);
}
