package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.govern.mapper.GovernanceDetailMapper;
import com.atguigu.yuntai.govern.service.GovernanceDetailService;
import com.atguigu.yuntai.govern.service.GovernanceOptionService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 治理明细 服务实现类
 * </p>
 */
@Service
public class GovernanceDetailServiceImpl extends ServiceImpl<GovernanceDetailMapper, GovernanceDetail> implements GovernanceDetailService {


    @Autowired
    GovernanceOptionService governanceOptionService;

    public List<GovernanceDetail> getGovernanceDetailListToFix(String optionType) {
        return this.baseMapper.getGovernanceDetailListToFix(optionType);
    }

    public Map<String,List<GovernanceDetail>> getGovernanceDetailMapToFixByType(  String optionType){
        List<GovernanceDetail> governanceDetailList = getGovernanceDetailListToFix(optionType);
        Map<String, List<GovernanceDetail>> governanceDetailMap = governanceDetailList.stream()
                .collect(Collectors.groupingBy(GovernanceDetail::getOptionCode));
        return governanceDetailMap;
    }

    public IPage<GovernanceDetail> getGovernanceDetailListByCode(Page<GovernanceDetail> pageParam, String optionCode) {
        return this.baseMapper.getGovernanceDetailListByCode(pageParam,optionCode);
    }



}
