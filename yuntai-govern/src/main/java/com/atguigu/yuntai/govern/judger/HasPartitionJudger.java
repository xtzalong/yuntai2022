package com.atguigu.yuntai.govern.judger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 分区数评分器
 */
@Component("HAS_PARTITION")
@Slf4j
public class HasPartitionJudger implements  Judger{

    //分区数code
    public   final String OPTION_CODE="HAS_PARTITION";
    //监控类型-规范
    public   final String OPTION_TYPE="SPEC";

    /**
     * 根据表中是否含有分区字段进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo, String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        //
        if( tableInfo.getPartitionColNameJson()!=null&&tableInfo.getPartitionColNameJson().length()>2){
            JSONArray jsonArray = JSON.parseArray(tableInfo.getPartitionColNameJson());
            governanceDetail.setGovernanceContent("分区数:"+jsonArray.size());
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else{
            governanceDetail.setGovernanceContent("缺少分区字段" );
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }

        return governanceDetail;
    }
}
