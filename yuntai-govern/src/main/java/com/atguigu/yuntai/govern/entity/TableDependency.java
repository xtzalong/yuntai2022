package com.atguigu.yuntai.govern.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *关系表
 */
@Data
@ApiModel(description = "关系表")
@TableName("table_dependency")
public class TableDependency implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 目标表
     */
    @ApiModelProperty(value = "目标表")
    @TableField("table_name_target")
    private String tableNameTarget;

    /**
     * 来源表
     */
    @ApiModelProperty(value = "来源表")
    @TableField("table_name_source")
    private String tableNameSource;

}
