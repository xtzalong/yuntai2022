package com.atguigu.yuntai.govern.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 治理项类别
 * </p>
 */
@Data
@ApiModel(description = "治理项类别")
@TableName("governance_type")
public class GovernanceType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 治理项类型编码
     */
    @ApiModelProperty(value = "治理项类型编码")
    @TableField("type_code")
    private String typeCode;

    /**
     * 治理项类型描述
     */
    @ApiModelProperty(value = "治理项类型描述")
    @TableField("type_desc")
    private String typeDesc;

    /**
     * 治理类型权重
     */
    @ApiModelProperty(value = "治理类型权重")
    @TableField("type_weight")
    private BigDecimal typeWeight;

}
