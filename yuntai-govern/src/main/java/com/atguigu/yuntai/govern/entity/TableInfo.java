package com.atguigu.yuntai.govern.entity;

import com.atguigu.yuntai.govern.entity.TableDependency;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 表元数据
 */
@Data
@ApiModel(description = "表元数据")
@TableName("table_info")
public class TableInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表id
     */
    @ApiModelProperty(value = "表id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名")
    @TableField("table_name")
    private String tableName;

    /**
     * 库名
     */
    @ApiModelProperty(value = "库名")
    @TableField("schema_name")
    private String schemaName;

    /**
     * 字段名json
     */
    @ApiModelProperty(value = "字段名json")
    @TableField("col_name_json")
    private String colNameJson;

    /**
     * 分区字段名json
     */
    @ApiModelProperty(value = "分区字段名json")
    @TableField("partition_col_name_json")
    private String partitionColNameJson;

    /**
     * 上游表名json
     */
    @ApiModelProperty(value = "上游表名json")
    @TableField("source_table_json")
    private String sourceTableJson;

    /**
     * hdfs所属人
     */
    @ApiModelProperty(value = "hdfs所属人")
    @TableField("table_fs_owner")
    private String tableFsOwner;

    /**
     * 参数信息
     */
    @ApiModelProperty(value = "参数信息")
    @TableField("table_parameters_json")
    private String tableParametersJson;

    /**
     * hdfs路径
     */
    @ApiModelProperty(value = "hdfs路径")
    @TableField("table_fs_path")
    private String tableFsPath;

    /**
     * 输入格式
     */
    @ApiModelProperty(value = "输入格式")
    @TableField("table_input_format")
    private String tableInputFormat;

    /**
     * 输出格式
     */
    @ApiModelProperty(value = "输出格式")
    @TableField("table_output_format")
    private String tableOutputFormat;

    /**
     * 行格式
     */
    @ApiModelProperty(value = "行格式")
    @TableField("table_row_format_serde")
    private String tableRowFormatSerde;

    /**
     * 表创建时间
     */
    @ApiModelProperty(value = "表创建时间")
    @TableField("table_create_time")
    private Date tableCreateTime;

    /**
     * 表类型
     */
    @ApiModelProperty(value = "表类型")
    @TableField("table_type")
    private String tableType;

    /**
     * 分桶列
     */
    @ApiModelProperty(value = "分桶列")
    @TableField("table_bucket_cols_json")
    private String tableBucketColsJson;

    /**
     * 分桶个数
     */
    @ApiModelProperty(value = "分桶个数")
    @TableField("table_bucket_num")
    private Integer tableBucketNum;

    /**
     * 排序列
     */
    @ApiModelProperty(value = "排序列")
    @TableField("table_sort_cols_json")
    private String  tableSortColsJson;

    /**
     * 数据量大小
     */
    @ApiModelProperty(value = "数据量大小")
    @TableField("table_size")
    private Long tableSize=0L;

    /**
     * 最后修改时间
     */
    @ApiModelProperty(value = "最后修改时间")
    @TableField("table_last_modify_time")
    private Date tableLastModifyTime;

    /**
     * 最后修改路径
     */
    @ApiModelProperty(value = "最后修改路径")
    @TableField("table_last_modify_path")
    private String tableLastModifyPath;

    /**
     * 最后修改路径大小
     */
    @ApiModelProperty(value = "最后修改路径大小")
    @TableField("table_last_modify_size")
    private Long tableLastModifySize;

    /**
     * 当前文件系统容量
     */
    @ApiModelProperty(value = "当前文件系统容量")
    @TableField("fs_capcity_size")
    private Long fsCapcitySize;

    /**
     * 当前文件系统使用量
     */
    @ApiModelProperty(value = "当前文件系统使用量")
    @TableField("fs_used_size")
    private Long fsUsedSize;

    /**
     * 当前文件系统剩余量
     */
    @ApiModelProperty(value = "当前文件系统剩余量")
    @TableField("fs_remain_size")
    private Long fsRemainSize;

    /**
     * 技术负责人
     */
    @ApiModelProperty(value = "技术负责人")
    @TableField("tec_owner_user_id")
    private Long tecOwnerUserId;

    /**
     * 业务负责人
     */
    @ApiModelProperty(value = "业务负责人")
    @TableField("busi_owner_user_id")
    private Long busiOwnerUserId;

    /**
     *  负责部门
     */
    @ApiModelProperty(value = "负责部门")
    @TableField("dept_id")
    private Long deptId;

    /**
     * 生命周期(天)
     */
    @ApiModelProperty(value = "生命周期")
    @TableField("lifecycle_days")
    private Long lifecycleDays;

    /**
     * 安全级别
     */
    @ApiModelProperty(value = "安全级别")
    @TableField("security_level")
    private String securityLevel;

    /**
     * 数仓所在层级(ODS\DWD\DIM\DWS\ADS)
     */
    @ApiModelProperty(value = "数仓所在层级(ODS\\DWD\\DIM\\DWS\\ADS)")
    @TableField("dw_level")
    private String dwLevel;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 表备注
     */
    @ApiModelProperty(value = "表备注")
    @TableField("table_comment")
    private String tableComment;
    /**
     *是否hdfs路径依存在
     */
    @ApiModelProperty(value = "是否hdfs路径依存在")
    @TableField("is_Hdfs_Path_exist")
    private String IsHdfsPathExist;

    /**
     *
     */
    @ApiModelProperty(value = "")
    @TableField(exist = false)
    private List<TableDependency> tableDependencyList;


    /**
     * SQL
     */
    @ApiModelProperty(value = "hiveSql")
    @TableField(exist = false)
    private String hiveSql;
}
