package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.TableDependency;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  表关系  Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface TableDependencyMapper extends BaseMapper<TableDependency> {

}
