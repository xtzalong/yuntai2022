package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 层级规范评分器
 */
@Component("IS_TABLE_LEVEL_SPEC")
@Slf4j
public class TableLevelSpecJudger implements  Judger{

    //层级规范
    public   final String OPTION_CODE="IS_TABLE_LEVEL_SPEC";
    //规范
    public   final String OPTION_TYPE="SPEC";

    /**
     * 根据是否有层级规范进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        String dwLevel = tableInfo.getDwLevel();

        if (dwLevel!=null &&dwLevel.length()>0){
            governanceDetail.setGovernanceContent("符合规范");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else {
            governanceDetail.setGovernanceContent("缺少层级配置" );
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }


        return governanceDetail;
    }
}
