package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.TableDependency;
import com.atguigu.yuntai.govern.mapper.TableDependencyMapper;
import com.atguigu.yuntai.govern.service.TableDependencyService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class TableDependencyServiceImpl extends ServiceImpl<TableDependencyMapper, TableDependency> implements TableDependencyService {

}
