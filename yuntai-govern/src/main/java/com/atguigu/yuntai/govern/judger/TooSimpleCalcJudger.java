package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.govern.parser.*;
import com.atguigu.yuntai.schedule.service.TDsTaskDefinitionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hive.ql.parse.ParseException;
import org.apache.hadoop.hive.ql.parse.SemanticException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * 是否过于简单检查评分器
 */
@Component("IS_TOO_SIMPLE")
@Slf4j
public class TooSimpleCalcJudger implements Judger {

    //是否过于简单(不含过滤 关联 分组)
    public   final String OPTION_CODE = "IS_TOO_SIMPLE";
    //计算
    public   final String OPTION_TYPE = "CALC";

    @Autowired
    TDsTaskDefinitionService tDsTaskDefinitionService;

    /**
     * 根据是否过于简单进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo, String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);
        String taskSql = tDsTaskDefinitionService.getTaskSqlByName(tableInfo.getTableName());

        if (taskSql == null || taskSql.length() == 0) {
            governanceDetail.setGovernanceContent("未发现任务sql");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
            return governanceDetail;
        }

        SqlParser sqlParser = null;
        Boolean hasGroupBy = false;
        Boolean hasJoin = false;
        Boolean hasAggFunction = false;

        Integer whereCount = 0;
        try {
            sqlParser = new SqlParser(taskSql);

            List<GroupbyObj> groupByObjList = sqlParser.getGroupByObjList();
            if (groupByObjList.size() > 0) {
                hasGroupBy = true;
            }

            List<JoinObj> joinObjList = sqlParser.getJoinObjList();
            if (joinObjList.size() > 0) {
                hasJoin = true;
            }


            List<FunctionObj> functionObjList = sqlParser.getFunctionObjList();
            for (FunctionObj functionObj : functionObjList) {
                if (functionObj.getIsAggFunction()) {
                    hasAggFunction = true;
                    break;
                }
            }


            List<WhereObj> whereObjList = sqlParser.getWhereObjList();
            for (WhereObj whereObj : whereObjList) {
                List<WhereObj.ConditionPair> conditionPairList = whereObj.getConditionPairList();
                whereCount += conditionPairList.size();
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SemanticException e) {
            e.printStackTrace();
        }

        if (hasGroupBy || hasJoin || hasAggFunction || whereCount > 0) {
            governanceDetail.setGovernanceContent("非简单加工");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        } else {
            governanceDetail.setGovernanceContent("简单加工");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }

        return governanceDetail;
    }
}
