package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 表备注评分器
 */
@Component("HAS_TABLE_COMMENT")
@Slf4j
public class TableCommentJudger implements Judger {

    //表备注
    public final String OPTION_CODE = "HAS_TABLE_COMMENT";
    //规范
    public final String OPTION_TYPE = "SPEC";

    /**
     * 根据是否有表备注进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo, String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);

        if (tableInfo.getTableComment() != null && tableInfo.getTableComment().length() > 2) {
            governanceDetail.setGovernanceContent("符合规范");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        } else {
            governanceDetail.setGovernanceContent("缺少表备注");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }

        return governanceDetail;
    }
}
