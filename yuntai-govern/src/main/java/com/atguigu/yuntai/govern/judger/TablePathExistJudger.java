package com.atguigu.yuntai.govern.judger;

import com.atguigu.yuntai.govern.entity.TableInfo;
import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * 是否文件路径被删评分器
 */
@Component("IS_FS_PATH_DELETE")
@Slf4j
public class TablePathExistJudger implements  Judger{

    //是否文件路径被删
    public   final String OPTION_CODE="IS_FS_PATH_DELETE";
    //存储
    public   final String OPTION_TYPE="STORAGE";

    /**
     * 根据文件路径是否被删进行评分
     * @param tableInfo
     * @param busiDate
     * @return
     */
    @Override
    public GovernanceDetail judge(TableInfo tableInfo,String busiDate) {
        GovernanceDetail governanceDetail = new GovernanceDetail(tableInfo);


        if (tableInfo.getIsHdfsPathExist()!=null&& tableInfo.getIsHdfsPathExist().equals("1") ){
            governanceDetail.setGovernanceContent("符合规范");
            governanceDetail.setGovernanceResult("1");
            governanceDetail.setGovernanceScore(BigDecimal.ONE);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }else {
            governanceDetail.setGovernanceContent("路径被删除");
            governanceDetail.setGovernanceResult("0");
            governanceDetail.setGovernanceScore(BigDecimal.ZERO);
            governanceDetail.setOptionCode(OPTION_CODE);
            governanceDetail.setOptionType(OPTION_TYPE);
        }


        return governanceDetail;
    }
}
