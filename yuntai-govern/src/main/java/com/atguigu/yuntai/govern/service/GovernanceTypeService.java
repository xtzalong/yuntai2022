package com.atguigu.yuntai.govern.service;

import com.atguigu.yuntai.govern.entity.GovernanceType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 治理项类别 服务类
 * </p>
 */
public interface GovernanceTypeService extends IService<GovernanceType> {

    /**
     * 查询治理项类别
     * @return
     */
    public Map<String , GovernanceType> getGovernanceTypeMap();

}
