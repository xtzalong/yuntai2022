package com.atguigu.yuntai.govern.service.impl;

import com.atguigu.yuntai.govern.entity.GovernanceUser;
import com.atguigu.yuntai.govern.mapper.GovernanceUserMapper;
import com.atguigu.yuntai.govern.service.GovernanceUserService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 技术负责人治理明细 服务实现类
 * </p>
 *
 */
@Service
public class GovernanceUserServiceImpl extends ServiceImpl<GovernanceUserMapper, GovernanceUser> implements GovernanceUserService {

}
