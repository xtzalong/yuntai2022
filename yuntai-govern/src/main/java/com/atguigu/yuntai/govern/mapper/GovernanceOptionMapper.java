package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.GovernanceOption;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 治理项 Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface GovernanceOptionMapper extends BaseMapper<GovernanceOption> {

}
