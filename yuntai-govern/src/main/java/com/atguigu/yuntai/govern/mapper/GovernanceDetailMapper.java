package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.GovernanceDetail;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 治理明细 Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface GovernanceDetailMapper extends BaseMapper<GovernanceDetail> {

    /**
     * 根据治理类型查询治理明细
     * @param optionType
     * @return
     */
    @Select("select * from governance_detail where option_type=#{option_type} and governance_result='0' " +
            "and busi_date=(select max(busi_date) from governance_detail )  ")
    public List<GovernanceDetail>  getGovernanceDetailListToFix(@Param("option_type") String optionType);

    /**
     * 根据治理编码查询治理明细
     * @param page
     * @param optionCode
     * @return
     */
    @Select("select * from governance_detail where option_code=#{option_code} and governance_result='0' " +
            "and busi_date=(select max(busi_date) from governance_detail )  ")
    public IPage<GovernanceDetail> getGovernanceDetailListByCode(Page<GovernanceDetail> page, @Param("option_code") String optionCode);
}
