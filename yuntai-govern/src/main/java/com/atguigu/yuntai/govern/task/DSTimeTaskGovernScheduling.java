package com.atguigu.yuntai.govern.task;

import com.atguigu.yuntai.govern.service.DSTimeTaskGovernService;
import org.springframework.scheduling.annotation.Scheduled;


/**
 * 需要在ds元数据库中创建一张表记录当天已预警记录
 * CREATE TABLE govern_alert_notified
 * (
 * id INT AUTO_INCREMENT PRIMARY KEY,
 * NAME VARCHAR(200),
 * task_code BIGINT,
 * task_type VARCHAR(50),
 * TIME DATETIME
 * )COMMENT='记录当天已预警的记录，定时器不必再查询表中数据'
 */


/**
 * DS定时任务类
 */
public class DSTimeTaskGovernScheduling {


    DSTimeTaskGovernService dsTimeTaskGovernService;

    /**
     * 5分钟 调度一次任务 检测DS调度器各任务的延迟情况
     */
    @Scheduled(cron = "0 */5 * * * ?")
    public void TaskTimeDelayGovernance() {
        dsTimeTaskGovernService.TaskTimeDelayGovernance();
    }


    @Scheduled(cron = "0 0 9 * * ?")
    public void QuotaGovernace() {
        dsTimeTaskGovernService.QuotaGovernance();
    }
}
