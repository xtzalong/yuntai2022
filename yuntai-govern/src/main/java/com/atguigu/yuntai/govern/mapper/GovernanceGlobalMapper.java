package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.GovernanceGlobal;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 治理全局情况 Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface GovernanceGlobalMapper extends BaseMapper<GovernanceGlobal> {

}
