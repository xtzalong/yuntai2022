package com.atguigu.yuntai.govern.mapper;

import com.atguigu.yuntai.govern.entity.GovernanceTable;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表治理明细 Mapper 接口
 * </p>
 */
@DS("mysql-yuntai-config")
public interface GovernanceTableMapper extends BaseMapper<GovernanceTable> {

}
