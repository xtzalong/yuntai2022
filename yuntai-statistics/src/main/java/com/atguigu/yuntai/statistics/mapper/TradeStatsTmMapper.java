package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStatsByTm;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 各品牌商品交易统计Mapper
 */
@DS("mysql-gmall-report")
public interface TradeStatsTmMapper extends BaseMapper<AdsTradeStatsByTm> {

    @Select("select * from ads_trade_stats_by_tm where  dt = #{vo.dt} and recent_days= #{vo.recentDays}")
    IPage<AdsTradeStatsByTm> selectPage(Page<AdsTradeStatsByTm> page, @Param("vo") QUnified qUnified);

    /**
     * 查询昨日各商品交易统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,tm_id,tm_name,order_count,order_user_count,order_refund_count,order_refund_user_count FROM ads_trade_stats_by_tm  WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsTradeStatsByTm> getAdsTradeStatsByTmYesterday();

    /**
     * 查询今日各商品交易统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,tm_id,tm_name,order_count,order_user_count,order_refund_count,order_refund_user_count FROM ads_trade_stats_by_tm  WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsTradeStatsByTm> getAdsTradeStatsByTmToday();
}
