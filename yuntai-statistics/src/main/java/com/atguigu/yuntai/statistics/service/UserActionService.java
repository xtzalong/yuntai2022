package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.common.util.EchartData;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsUserAction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户行为分析服务接口
 */
public interface UserActionService extends IService<AdsUserAction> {

    /**
     * 根据dt和recent_days查询用户行为分析生成漏斗图
     *
     * @param qUnified
     * @return
     */
    EchartData getUserActionConvert(QUnified qUnified);
}
