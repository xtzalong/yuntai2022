package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QCoupon;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.atguigu.yuntai.statistics.entity.AdsCouponStats;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 优惠卷分析服务接口
 */
public interface CouponService extends IService<AdsCouponStats> {

    /**
     * 根据开始时间查询优惠卷名称
     *
     * @param qCoupon
     * @return
     */
    List<AdsCouponStats> getCouponNameByStartDate(QCoupon qCoupon);


    /**
     * 根据开始时间和优惠券名称查询优惠券统计
     *
     * @param qCoupon
     * @return
     */
    IPage<AdsCouponStats> selectPage(Page<AdsCouponStats> pageParam, QCoupon qCoupon);
}
