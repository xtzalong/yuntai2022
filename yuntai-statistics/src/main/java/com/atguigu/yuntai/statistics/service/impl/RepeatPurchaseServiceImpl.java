package com.atguigu.yuntai.statistics.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.yuntai.common.util.EchartsConverter;
import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsRepeatPurchaseByTm;
import com.atguigu.yuntai.statistics.mapper.RepeatPurchaseMapper;
import com.atguigu.yuntai.statistics.service.RepeatPurchaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 最近7/30日各品牌复购率实现类
 */
@Service
public class RepeatPurchaseServiceImpl extends ServiceImpl<RepeatPurchaseMapper, AdsRepeatPurchaseByTm> implements RepeatPurchaseService {

    public JSONObject getTmRepeat(QUnified qUnified, int showNum) {
        List<NameValueData> tmRepeatList = this.baseMapper.getTmRepeat(qUnified.getRecentDays(), qUnified.getDt(), showNum);
        if (CollectionUtils.isEmpty(tmRepeatList)) {
            tmRepeatList = new ArrayList<>();
        }
        return EchartsConverter.converterFromNameValue(tmRepeatList, false);

    }
}
