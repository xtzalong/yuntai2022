package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;


@Data
@ApiModel(description = "用户行为漏斗分析")
@TableName("ads_user_action")
public class AdsUserAction  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private Date dt;

    @ApiModelProperty(value = "最近天数,1:最近1天,7:最近7天,30:最近30天")
    @TableField("recent_days")
    private long recentDays;

    @ApiModelProperty(value = "浏览首页人数")
    @TableField("home_count")
//    @Builder.Default
    private Long homeCount = 0l;

    @ApiModelProperty(value = "浏览商品详情页人数")
    @TableField("good_detail_count")
//    @Builder.Default
    private Long goodDetailCount = 0l;

    @ApiModelProperty(value = "加入购物车人数")
    @TableField("cart_count")
//    @Builder.Default
    private Long cartCount = 0l;

    @ApiModelProperty(value = "下单人数")
    @TableField("order_count")
//    @Builder.Default
    private Long orderCount = 0l;

    @ApiModelProperty(value = "支付人数")
    @TableField("payment_count")
//    @Builder.Default
    private Long paymentCount = 0l;


}
