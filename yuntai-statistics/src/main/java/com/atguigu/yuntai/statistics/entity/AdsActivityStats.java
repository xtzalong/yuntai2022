package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 最近30天发布的活动的补贴率实体类
 */
@Data
@ApiModel(description = "最近30天发布的活动的补贴率")
@TableName("ads_activity_stats")
public class AdsActivityStats  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;


    @ApiModelProperty(value = "活动ID")
    @TableField("activity_id")
    private String activityId;


    @ApiModelProperty(value = "活动名称")
    @TableField("activity_name")
    private String activityName;


    @ApiModelProperty(value = "活动开始日期")
    @TableField("start_date")
    private String startDate;


    @ApiModelProperty(value = "补贴率")
    @TableField("reduce_rate")
    private BigDecimal reduceRate;

}
