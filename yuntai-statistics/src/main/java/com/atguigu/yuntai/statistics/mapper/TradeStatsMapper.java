package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsTradeStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 交易综合统计Mapper
 */
@DS("mysql-gmall-report")
public interface TradeStatsMapper extends BaseMapper<AdsTradeStats> {
    /**
     * 查询昨日综合交易
     *
     * @return
     */
    @Select("SELECT dt,recent_days,order_total_amount,order_count,order_count,order_user_count,order_refund_count,order_refund_user_count FROM ads_trade_stats WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsTradeStats> getAdsTradeStatsYesterday();

    /**
     * 查询今日总合计交易
     *
     * @return
     */
    @Select("SELECT dt,recent_days,order_total_amount,order_count,order_count,order_user_count,order_refund_count,order_refund_user_count FROM ads_trade_stats WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsTradeStats> getAdsTradeStatsToday();

}
