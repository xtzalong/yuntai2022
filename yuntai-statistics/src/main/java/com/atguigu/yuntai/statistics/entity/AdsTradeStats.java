package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: gmall
 * @description:交易统计实体类
 */
@Data
@ApiModel(description = "交易综合统计")
@TableName("ads_trade_stats")
public class AdsTradeStats implements Serializable {


    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;

    /**
     * 最近天数,1:最近1日,7:最近7天,30:最近30天
     */
    @ApiModelProperty(value = "最近天数")
    @TableField("recent_days")
    private int recentDays;

    @ApiModelProperty(value = "订单总额")
    @TableField("order_total_amount")
    private String orderTotalAmount;


    @ApiModelProperty(value = "订单数")
    @TableField("order_count")
    private int orderCount;

    @ApiModelProperty(value = "下单人数")
    @TableField("order_user_count")
    private int orderUserCount;


    @ApiModelProperty(value = "退单数")
    @TableField("order_refund_count")
    private int orderRefundCount;


    @ApiModelProperty(value = "退单人数")
    @TableField("order_refund_user_count")
    private int orderRefundUserCount;
}
