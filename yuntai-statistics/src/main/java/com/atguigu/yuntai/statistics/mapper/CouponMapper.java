package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.bean.QCoupon;
import com.atguigu.yuntai.statistics.entity.AdsCouponStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 最近30天发布的优惠券的补贴率
 */
@DS("mysql-gmall-report")
public interface CouponMapper extends BaseMapper<AdsCouponStats> {

    @Select("select * from ads_coupon_stats where  dt = #{vo.startDate} and coupon_name= #{vo.couponName}")
    IPage<AdsCouponStats> selectPage(Page<AdsCouponStats> page, @Param("vo") QCoupon qCoupon);
}
