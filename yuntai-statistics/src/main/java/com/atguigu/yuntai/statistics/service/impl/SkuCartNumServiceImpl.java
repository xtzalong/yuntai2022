package com.atguigu.yuntai.statistics.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.yuntai.common.util.EchartsConverter;
import com.atguigu.yuntai.common.util.NameValueData;
import com.atguigu.yuntai.statistics.entity.AdsSkuCartNumTop;
import com.atguigu.yuntai.statistics.mapper.SkuCartNumTop3Mapper;
import com.atguigu.yuntai.statistics.service.SkuCartNumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 各分类商品购物车存量Top实现类
 */
@Service
public class SkuCartNumServiceImpl extends ServiceImpl<SkuCartNumTop3Mapper, AdsSkuCartNumTop> implements SkuCartNumService {


    @Override
    public List<NameValueData> getCategory1(String curDate) {
        List<NameValueData> category1 = this.baseMapper.getCategory1(curDate);
        if (CollectionUtils.isNotEmpty(category1)) {
            return category1;
        }
        return new ArrayList<>();
    }


    @Override
    public List<NameValueData> getCategory2(String curDate, String category1Id) {
        List<NameValueData> category2 = this.baseMapper.getCategory2(curDate, category1Id);
        if (CollectionUtils.isNotEmpty(category2)) {
            return category2;
        }
        return new ArrayList<>();
    }


    @Override
    public List<NameValueData> getCategory3(String curDate, String category1Id, String category2Id) {
        List<NameValueData> category3 = this.baseMapper.getCategory3(curDate, category1Id, category2Id);
        if (CollectionUtils.isNotEmpty(category3)) {
            return category3;
        }
        return new ArrayList<>();
    }


    @Override
    public JSONObject getTmTopNData(String curDate, String category1Id, String category2Id, String category3Id) {
        List<NameValueData> tmTopNData = this.baseMapper.getTmTopNData(curDate, category1Id, category2Id, category3Id);
        if (CollectionUtils.isEmpty(tmTopNData)) {
            tmTopNData = new ArrayList<>();
        }
        return EchartsConverter.converterFromNameValue(tmTopNData, false);
    }
}
