package com.atguigu.yuntai.statistics.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ReturnCategory implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    Long category3Id;
    @ApiModelProperty(value = "统计日期")
    String category3Name;

    @ApiModelProperty(value = "统计日期")
    BigDecimal orderAmount ;

}
