package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.ReturnCategory;
import com.atguigu.yuntai.statistics.bean.ReturnSpu;
import com.atguigu.yuntai.statistics.entity.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;

/**
 * sugar实时服务接口
 */
public interface SugarServer extends IService<AdsActivityStats> {

    /**
     * 获取某一天的总交易额
     * @param date
     * @return
     */
    public BigDecimal getGMV(int date);


    /**
     * 统计某天不同SPU商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    public List<ReturnSpu> getProductStatsGroupBySpu(int date, int limit);


    /**
     * 统计某天不同类别商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    public List<ReturnCategory> getProductStatsGroupByCategory3(int date, int limit);


    /**
     * 统计某天不同品牌商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    public List<ProductStats> getProductStatsByTrademark(int date, int limit);

    /**
     * 地区
     * @param date
     * @return
     */
    public List<ProvinceStats> getProvinceStats(int date);

    /**
     * 新老用户对比
     * @param date
     * @return
     */
    public List<VisitorStats> getVisitorStatsByNewFlag(int date);

    /**
     * 用户访问时分
     * @param date
     * @return
     */
    public List<VisitorStats> getVisitorStatsByHour(int date);

    /**
     * pv
     * @param date
     * @return
     */
    public Long getPv(int date);

    /**
     * uv
     * @param date
     * @return
     */
    public Long getUv(int date);

    /**
     * 热门词汇
     * @param date
     * @param limit
     * @return
     */
    public List<KeywordStats> getKeywordStats(int date, int limit);


}
