package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTrafficStatsByChannel;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 各渠道流量统计Mapper
 */

@DS("mysql-gmall-report")
public interface TrafficStatsByChannelMapper extends BaseMapper<AdsTrafficStatsByChannel> {

    @Select("select * from ads_traffic_stats_by_channel where  dt = #{vo.dt} and recent_days= #{vo.recentDays}")
    IPage<AdsTrafficStatsByChannel> selectPage(Page<AdsTrafficStatsByChannel> page, @Param("vo") QUnified qUnified);

    /**
     * 查询昨日各渠道流量统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,channel,uv_count,avg_duration_sec,avg_page_count,sv_count,bounce_rate FROM ads_traffic_stats_by_channel  WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsTrafficStatsByChannel> getAdsTrafficStatsByChannelYesterday();

    /**
     * 查询今日渠道流量统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,channel,uv_count,avg_duration_sec,avg_page_count,sv_count,bounce_rate FROM ads_traffic_stats_by_channel  WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsTrafficStatsByChannel> getAdsTrafficStatsByChannelToday();
}
