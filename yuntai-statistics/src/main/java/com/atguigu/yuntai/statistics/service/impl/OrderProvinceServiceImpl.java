package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsOrderByProvince;
import com.atguigu.yuntai.statistics.mapper.OrderProvinceMapper;
import com.atguigu.yuntai.statistics.service.OrderProvinceService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 各省份交易统计实现类
 */
@Service
public class OrderProvinceServiceImpl extends ServiceImpl<OrderProvinceMapper, AdsOrderByProvince> implements OrderProvinceService {


    @Override
    public List<Map> getOrderNumProvinceData(QUnified qUnified) {

        List<Map> orderProvinceList = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        List<AdsOrderByProvince> orderProvinceDataList =
                this.baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(orderProvinceDataList)) {
            for (AdsOrderByProvince adsOrderByProvince : orderProvinceDataList) {
                Map<String, String> orderProvinceMap = new HashMap<>();
                if (StringUtils.isNotEmpty(adsOrderByProvince.getProvinceName()) && adsOrderByProvince.getOrderCount() != 0) {
                    orderProvinceMap.put("name", adsOrderByProvince.getProvinceName());
                    orderProvinceMap.put("value", Integer.toString(adsOrderByProvince.getOrderCount()));
                    orderProvinceList.add(orderProvinceMap);
                }

            }
        }
        return orderProvinceList;
    }


    @Override
    public List<Map> getOrderMoneyProvinceData(QUnified qUnified) {
        List<Map> orderProvinceList = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("recent_days", qUnified.getRecentDays());
        queryWrapper.eq("dt", qUnified.getDt());
        List<AdsOrderByProvince> orderProvinceDataList =
                this.baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(orderProvinceDataList)) {
            for (AdsOrderByProvince adsOrderByProvince : orderProvinceDataList) {
                Map<String, String> orderProvinceMap = new HashMap<>();
                if (StringUtils.isNotEmpty(adsOrderByProvince.getProvinceName()) && adsOrderByProvince.getOrderTotalAmount() != null) {
                    orderProvinceMap.put("name", adsOrderByProvince.getProvinceName());
                    orderProvinceMap.put("value", adsOrderByProvince.getOrderTotalAmount().toString());
                    orderProvinceList.add(orderProvinceMap);
                }

            }
        }
        return orderProvinceList;
    }
}
