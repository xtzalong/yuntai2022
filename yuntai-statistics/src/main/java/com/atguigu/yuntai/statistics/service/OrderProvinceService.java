package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsOrderByProvince;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 各省份交易统计服务接口
 */
public interface OrderProvinceService extends IService<AdsOrderByProvince> {

    /**
     * 获取各省份的订单数交易统计
     *
     * @param qUnified
     * @return
     */
    List<Map> getOrderNumProvinceData(QUnified qUnified);

    /**
     * 各省份订单金额交易统计
     *
     * @param qUnified
     * @return
     */
    List<Map> getOrderMoneyProvinceData(QUnified qUnified);
}
