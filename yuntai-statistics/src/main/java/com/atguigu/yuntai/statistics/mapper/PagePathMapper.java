package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsPagePath;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户路径分析Mapper
 */
@DS("mysql-gmall-report")
public interface PagePathMapper extends BaseMapper<AdsPagePath> {

}
