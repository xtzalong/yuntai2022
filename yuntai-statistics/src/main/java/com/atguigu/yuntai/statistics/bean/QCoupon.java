package com.atguigu.yuntai.statistics.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: gmall
 * @description:优惠券查询条件实体类
 */
@Data
@ApiModel(description = "优惠券查询条件")
public class QCoupon implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "优惠卷开始日期")
    private String startDate;

    @ApiModelProperty(value = "优惠卷名称")
    private String couponName;
}
