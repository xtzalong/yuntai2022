package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: gmall
 * @description: 各渠道流量统计实体类
 */
@Data
@ApiModel(description = "各渠道流量统计")
@TableName("ads_traffic_stats_by_channel")
public class AdsTrafficStatsByChannel implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;

    /**
     * 最近天数,1:最近1天,7:最近7天,30:最近30天
     */
    @ApiModelProperty(value = "最近天数")
    @TableField("recent_days")
    private int recentDays;


    @ApiModelProperty(value = "渠道")
    @TableField("channel")
    private String channel;


    @ApiModelProperty(value = "访客人数")
    @TableField("uv_count")
    private int uvCount;


    @ApiModelProperty(value = "会话平均停留时长，单位为秒")
    @TableField("avg_duration_sec")
    private int avgDurationSec;


    @ApiModelProperty(value = "会话平均浏览页面数")
    @TableField("avg_page_count")
    private int avgPageCount;


    @ApiModelProperty(value = "会话数")
    @TableField("sv_count")
    private int svCount;


    @ApiModelProperty(value = "跳出率")
    @TableField("bounce_rate")
    private BigDecimal bounceRate;

}
