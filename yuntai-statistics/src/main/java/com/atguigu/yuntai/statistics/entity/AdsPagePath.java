package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;


@Data
@ApiModel(description = "路径分析")
@TableName("ads_page_path")
public class AdsPagePath  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private Date dt;

    @ApiModelProperty(value = "最近天数,1:最近1天,7:最近7天,30:最近30天")
    @TableField("recent_days")
    private long recentDays;

    @ApiModelProperty(value = "跳转起始页面ID")
    @TableField("source")
    private String source;

    @ApiModelProperty(value = "跳转终到页面ID")
    @TableField("target")
    private String target;

    @ApiModelProperty(value = "跳转次数")
    @TableField("path_count")
    private Long pathCount;

}
