package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 各分类商品购物车存量实体类
 */
@Data
@ApiModel(description = "各分类商品购物车存量")
@TableName("ads_order_by_province")
public class AdsSkuCartNumTop  implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;


    @ApiModelProperty(value = "一级分类id")
    @TableField("category1_id")
    private String category1Id;


    @ApiModelProperty(value = "一级分类名称")
    @TableField("category1_name")
    private String category1Name;


    @ApiModelProperty(value = "二级分类id")
    @TableField("category2_id")
    private String category2Id;


    @ApiModelProperty(value = "二分类名称")
    @TableField("category2_name")
    private String category2Name;


    @ApiModelProperty(value = "三级分类id")
    @TableField("category3_id")
    private String category3Id;


    @ApiModelProperty(value = "三级分类名称")
    @TableField("category3_name")
    private String category3Name;


    @ApiModelProperty(value = "商品id")
    @TableField("sku_id")
    private String skuId;


    @ApiModelProperty(value = "商品名称")
    @TableField("sku_name")
    private String skuName;


    @ApiModelProperty(value = "购物车中商品数量")
    @TableField("cart_num")
    private int cart_num;


    @ApiModelProperty(value = "排名")
    @TableField("rk")
    private int rk;
}
