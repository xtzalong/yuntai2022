package com.atguigu.yuntai.statistics.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStatsByCate;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 各品类商品交易服务接口
 */
public interface TradeStatsCateService extends IService<AdsTradeStatsByCate> {


    /**
     * 各品类商品交易统计
     *
     * @param qUnified
     * @return
     */
    List<JSONObject> getCateTradeStats(QUnified qUnified);

}
