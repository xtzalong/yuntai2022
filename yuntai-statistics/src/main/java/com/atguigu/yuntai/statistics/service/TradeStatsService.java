package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStats;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 交易综合统计服务接口
 */
public interface TradeStatsService extends IService<AdsTradeStats> {
    /**
     * 获取订单的昨天、近7天以及近30天的订单数、GMV、下单人数
     *
     * @param qUnified
     * @return
     */
    AdsTradeStats getTradeByDaysAndDt(QUnified qUnified);
}
