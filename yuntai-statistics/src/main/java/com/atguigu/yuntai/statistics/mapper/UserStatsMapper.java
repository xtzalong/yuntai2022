package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsUserStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户新增活跃统计Mapper
 */
@DS("mysql-gmall-report")
public interface UserStatsMapper extends BaseMapper<AdsUserStats> {

    /**
     * 查询昨天用户新增表
     *
     * @return
     */
    @Select("SELECT dt,recent_days,new_user_count,active_user_count FROM ads_user_stats WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsUserStats> getAdsUserStatsYesterday();

    /**
     * 查询今天用户新增表
     *
     * @return
     */
    @Select("SELECT dt,recent_days,new_user_count,active_user_count FROM ads_user_stats WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsUserStats> getAdsUserStateToday();

}
