package com.atguigu.yuntai.statistics.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsRepeatPurchaseByTm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 最近7/30日各品牌复购率服务接口
 */
public interface RepeatPurchaseService extends IService<AdsRepeatPurchaseByTm> {


    /**
     * 最近7/30日各品牌复购率
     *
     * @param qUnified
     * @param showNum
     * @return
     */
    JSONObject getTmRepeat(QUnified qUnified, int showNum);
}
