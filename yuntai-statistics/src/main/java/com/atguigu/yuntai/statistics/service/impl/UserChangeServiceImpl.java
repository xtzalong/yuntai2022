package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsUserChange;
import com.atguigu.yuntai.statistics.mapper.UserChangeMapper;
import com.atguigu.yuntai.statistics.service.UserChangeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户变动统计实现类
 */
@Service
public class UserChangeServiceImpl extends ServiceImpl<UserChangeMapper, AdsUserChange> implements UserChangeService {



    @Override
    public AdsUserChange getUserChange(QUnified qUnified) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("dt", qUnified.getDt());
        return this.baseMapper.selectOne(queryWrapper);
    }
}
