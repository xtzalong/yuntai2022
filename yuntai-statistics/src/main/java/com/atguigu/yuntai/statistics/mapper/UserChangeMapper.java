package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsUserChange;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * 用户变动统计Mapper
 */
@DS("mysql-gmall-report")
public interface UserChangeMapper extends BaseMapper<AdsUserChange> {


    /**
     * 查询昨日用户变动
     *
     * @return
     */
    @Select("SELECT dt,user_churn_count,user_back_count FROM ads_user_change WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public AdsUserChange getAdsUserChangeYesterday();

    /**
     * 查询今日用户变动
     *
     * @return
     */
    @Select("SELECT dt,user_churn_count,user_back_count FROM ads_user_change WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public AdsUserChange getAdsUserChangeToday();
}
