package com.atguigu.yuntai.statistics.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ReturnSpu implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "统计日期")
    Long spuId;
    @ApiModelProperty(value = "统计日期")
    String spuName;
    @ApiModelProperty(value = "统计日期")
    BigDecimal orderAmount ;

    @ApiModelProperty(value = "统计日期")
    Long orderCt ;

}
