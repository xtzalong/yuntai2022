package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.ReturnCategory;
import com.atguigu.yuntai.statistics.bean.ReturnSpu;
import com.atguigu.yuntai.statistics.entity.*;
import com.atguigu.yuntai.statistics.mapper.*;
import com.atguigu.yuntai.statistics.service.SugarServer;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * sugar实时实现类
 */
@Service
public class SugarServerImpl extends ServiceImpl<ActivityMapper, AdsActivityStats> implements SugarServer {

    @Autowired
    ProductStatsMapper productStatsMapper;

    @Autowired
    ProvinceStatsMapper provinceStatsMapper;

    @Autowired
    VisitorStatsMapper visitorStatsMapper;

    @Autowired
    KeywordStatsMapper keywordStatsMapper;

    @Override
    public BigDecimal getGMV(int date) {
        return productStatsMapper.getGMV(date);
    }


    @Override
    public List<ReturnSpu> getProductStatsGroupBySpu(int date, int limit) {
        return productStatsMapper.getProductStatsGroupBySpu(date, limit);
    }

    @Override
    public List<ReturnCategory> getProductStatsGroupByCategory3(int date, int limit) {
        return productStatsMapper.getProductStatsGroupByCategory3(date, limit);
    }

    @Override
    public List<ProductStats> getProductStatsByTrademark(int date, int limit) {
        return productStatsMapper.getProductStatsByTrademarkSugar(date, limit);
    }

    @Override
    public List<ProvinceStats> getProvinceStats(int date) {
        return provinceStatsMapper.selectProvinceStats(date);
    }

    @Override
    public List<VisitorStats> getVisitorStatsByNewFlag(int date) {
        return visitorStatsMapper.selectVisitorStatsByNewFlag(date);
    }

    @Override
    public List<VisitorStats> getVisitorStatsByHour(int date) {
        return visitorStatsMapper.selectVisitorStatsByHour(date);
    }

    @Override
    public Long getPv(int date) {
        return visitorStatsMapper.selectPv(date);
    }

    @Override
    public Long getUv(int date) {
        return visitorStatsMapper.selectUv(date);
    }

    @Override
    public List<KeywordStats> getKeywordStats(int date, int limit) {
        return keywordStatsMapper.selectKeywordStats(date, limit);
    }

}
