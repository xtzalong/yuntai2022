package com.atguigu.yuntai.statistics.mapper;

import com.atguigu.yuntai.statistics.entity.AdsNewBuyerStats;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 新增交易用户统计Mapper
 */

@DS("mysql-gmall-report")
public interface UserNewBuyerStatsMapper extends BaseMapper<AdsNewBuyerStats> {
    /**
     * 查询昨日新增交易用户统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,new_order_user_count,new_payment_user_count FROM ads_new_buyer_stats WHERE TO_DAYS(NOW())-TO_DAYS(dt)=1")
    public List<AdsNewBuyerStats> getAdsNewBuyerStatsYesterday();

    /**
     * 查询今日新增交易用户统计
     *
     * @return
     */
    @Select("SELECT dt,recent_days,new_order_user_count,new_payment_user_count FROM ads_new_buyer_stats WHERE TO_DAYS(NOW())=TO_DAYS(dt)")
    public List<AdsNewBuyerStats> getAdsNewBuyerStatsToday();
}
