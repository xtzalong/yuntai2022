package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 各地区订单统计实体类
 */
@Data
@ApiModel(description = "各省份交易统计")
@TableName("ads_order_by_province")
public class AdsOrderByProvince implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;

    /**
     * 最近天数,1:最近1天,7:最近7天,30:最近30天
     */
    @ApiModelProperty(value = "统计日期")
    @TableField("recent_days")
    private int recentDays;

    @ApiModelProperty(value = "省份ID")
    @TableField("province_id")
    private String provinceId;


    @ApiModelProperty(value = "省份名称")
    @TableField("province_name")
    private String provinceName;


    @ApiModelProperty(value = "地区编码")
    @TableField("area_code")
    private String areaCode;


    @ApiModelProperty(value = "国际标准地区编码")
    @TableField("iso_code")
    private String isoCode;


    @ApiModelProperty(value = "国际标准地区编码")
    @TableField("iso_code_3166_2")
    private String isoCode31662;


    @ApiModelProperty(value = "订单数")
    @TableField("order_count")
    private int orderCount;


    @ApiModelProperty(value = "订单金额")
    @TableField("order_total_amount")
    private BigDecimal orderTotalAmount;

}
