package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStatsByTm;
import com.atguigu.yuntai.statistics.mapper.TradeStatsTmMapper;
import com.atguigu.yuntai.statistics.service.TradeStatsTmService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 各品牌商品交易统计实现类
 */
@Service
public class TradeStatsTmServiceImpl extends ServiceImpl<TradeStatsTmMapper, AdsTradeStatsByTm> implements TradeStatsTmService {



    @Override
    public IPage<AdsTradeStatsByTm> selectPage(Page<AdsTradeStatsByTm> pageParam, QUnified qUnified) {
        return this.baseMapper.selectPage(pageParam, qUnified);
    }
}
