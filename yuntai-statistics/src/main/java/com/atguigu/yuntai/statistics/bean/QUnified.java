package com.atguigu.yuntai.statistics.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 统一查询条件
 */
@Data
@ApiModel(description = "统一查询条件")
public class QUnified implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "查询时间")
    private String dt;


    @ApiModelProperty(value = "查询天数")
    private int recentDays;

}
