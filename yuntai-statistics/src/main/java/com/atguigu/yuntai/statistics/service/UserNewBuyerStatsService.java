package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsNewBuyerStats;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * 新增交易用户统计实现接口
 */
public interface UserNewBuyerStatsService extends IService<AdsNewBuyerStats> {

    /**
     * 根据dt和recen_day查询新增交易用户
     *
     * @param qUnified
     * @return
     */
    AdsNewBuyerStats getNewBuyerStats(QUnified qUnified);
}
