package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.entity.AdsUserRetention;
import com.atguigu.yuntai.statistics.mapper.UserRetentionMapper;
import com.atguigu.yuntai.statistics.service.UserRetentionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户留存率实现类
 */
@Service
public class UserRetentionServiceImpl extends ServiceImpl<UserRetentionMapper, AdsUserRetention> implements UserRetentionService {


    @Override
    public List<Map> getUseRetention(String dt) {

        List<Map> useRetentionList = this.baseMapper.getUserRetention(dt);
        if (CollectionUtils.isEmpty(useRetentionList)) {
            useRetentionList = new ArrayList<>();
        }
        return useRetentionList;
    }
}
