package com.atguigu.yuntai.statistics.service.impl;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTrafficStatsByChannel;
import com.atguigu.yuntai.statistics.mapper.TrafficStatsByChannelMapper;
import com.atguigu.yuntai.statistics.service.TrStatsChannelService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 各渠道流量统计实现类
 */

@Service
public class TrStatsByChannelServiceImpl extends ServiceImpl<TrafficStatsByChannelMapper, AdsTrafficStatsByChannel> implements TrStatsChannelService {




    /**
     * 各渠道流量统计分页列表
     *
     * @param pageParam
     * @param qUnified
     * @return
     */
    @Override
    public IPage<AdsTrafficStatsByChannel> selectPage(Page<AdsTrafficStatsByChannel> pageParam, QUnified qUnified) {
        return this.baseMapper.selectPage(pageParam, qUnified);
    }

}

