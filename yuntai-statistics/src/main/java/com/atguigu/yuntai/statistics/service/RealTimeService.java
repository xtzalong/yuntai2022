package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.ReturnSpu;
import com.atguigu.yuntai.statistics.bean.ReturnVisitorStats;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 实时服务接口
 */
public interface RealTimeService {

    /**
     * 获取某一天的总交易额
     * @param date
     * @return
     */
    public BigDecimal getGMV(int date);

    /**
     * 统计某天不同SPU商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    public List<ReturnSpu> getProductStatsGroupBySpu(int date, int limit);

    /**
     * 统计某天不同类别商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    public String getProductStatsGroupByCategory3(int date, int limit);

    /**
     * 统计某天不同品牌商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    public String getProductStatsByTrademark(int date, int limit);

    /**
     *
     * @param date
     * @return
     */
    public List<Map> getProvinceStats(int date);

    /**
     * 地区
     * @param date
     * @return
     */
    List<ReturnVisitorStats> getVisitorStatsByNewFlag(int date);

    /**
     * 新老用户对比
     * @param date
     * @return
     */
    public String getVisitorStatsByHour(int date);

    /**
     * pv
     * @param date
     * @return
     */
    public Long getPv(int date);

    /**
     * uv
     * @param date
     * @return
     */
    public Long getUv(int date);

    /**
     * 热门词汇
     * @param date
     * @param limit
     * @return
     */
    public String getKeywordStats(int date, int limit);


}
