package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTrafficStatsByChannel;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 各渠道流量统计服务接口
 */
public interface TrStatsChannelService extends IService<AdsTrafficStatsByChannel> {


    /**
     * 各渠道流量统计分页列表
     *
     * @param pageParam
     * @param qUnified
     * @return
     */
    IPage<AdsTrafficStatsByChannel> selectPage(Page<AdsTrafficStatsByChannel> pageParam, QUnified qUnified);
}
