package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "用户变动")
@TableName("ads_user_change")
public class AdsUserChange  implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;

    @ApiModelProperty(value = "流失用户数")
    @TableField("user_churn_count")
    private String userChurnCount;

    @ApiModelProperty(value = "回流用户数")
    @TableField("user_back_count")
    private String userBackCount;


}
