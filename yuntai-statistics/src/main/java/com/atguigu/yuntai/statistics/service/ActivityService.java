package com.atguigu.yuntai.statistics.service;

import com.atguigu.yuntai.statistics.bean.QActivity;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 活动分析服务接口
 */
public interface ActivityService extends IService<AdsActivityStats> {
    /**
     * 根据时间查询活动名称
     *
     * @param qActivity
     * @return
     */
    List<AdsActivityStats> getActivityNameByStartDate(QActivity qActivity);

    /**
     * 根据时间名称和dt查询所有
     *
     * @param qActivity
     * @return
     */
    IPage<AdsActivityStats> selectPage(Page<AdsActivityStats> pageParam, QActivity qActivity);
}
