package com.atguigu.yuntai.statistics.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 各品牌商品交易统计表实体类
 */
@Data
@ApiModel(description = "各品牌商品交易统计")
@TableName("ads_trade_stats_by_tm")
public class AdsTradeStatsByTm implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计日期")
    @TableField("dt")
    private String dt;

    /**
     * 最近天数,1:最近1天,7:最近7天,30:最近30天
     */
    @ApiModelProperty(value = "最近天数")
    @TableField("recent_days")
    private int recentDays;


    @ApiModelProperty(value = "品牌ID")
    @TableField("tm_id")
    private String tmId;


    @ApiModelProperty(value = "品牌名称")
    @TableField("tm_name")
    private String tmName;


    @ApiModelProperty(value = "订单数")
    @TableField("order_count")
    private int orderCount;


    @ApiModelProperty(value = "订单人数")
    @TableField("order_user_count")
    private int orderUserCount;


    @ApiModelProperty(value = "退单数")
    @TableField("order_refund_count")
    private int orderRefundCount;


    @ApiModelProperty(value = "退单人数")
    @TableField("order_refund_user_count")
    private int orderRefundUserCount;

}
