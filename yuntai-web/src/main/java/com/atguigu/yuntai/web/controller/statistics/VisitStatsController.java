package com.atguigu.yuntai.web.controller.statistics;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTrafficStatsByChannel;
import com.atguigu.yuntai.statistics.service.PagePathService;
import com.atguigu.yuntai.statistics.service.TrStatsChannelService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 流量统计controller
 */
@RestController
@RequestMapping("/statistics/visit")
@Api(tags = "离线指标")
//@CrossOrigin //跨域
@Slf4j
public class VisitStatsController {

    @Autowired
    private PagePathService pagePathService;

    @Autowired
    private TrStatsChannelService trStatsChannelService;


    /**
     * 用户路径分析
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "各渠道访问流量统计")
    @GetMapping("getPagePath")
    @CrossOrigin
    public Result getPagePath(
            @ApiParam(name = "qUnified", value = "最近n日", required = true)
                    QUnified qUnified) {
        return Result.ok(pagePathService.getPagePathCount(qUnified));
    }

    /**
     * 各渠道访问流量统计
     *
     * @param page
     * @param limit
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "用户路径分析")
    @GetMapping("getTrafficStats/{page}/{limit}")
    @CrossOrigin
    public Result getTrafficStats(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        Page<AdsTrafficStatsByChannel> pageParam = new Page<>(page, limit);
        IPage<AdsTrafficStatsByChannel> pageModel = trStatsChannelService.selectPage(pageParam, qUnified);
        return Result.ok(pageModel);
    }
}


