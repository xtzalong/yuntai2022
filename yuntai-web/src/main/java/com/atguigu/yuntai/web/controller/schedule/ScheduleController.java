package com.atguigu.yuntai.web.controller.schedule;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.schedule.bean.QTaskInstance;
import com.atguigu.yuntai.schedule.bean.TaskInstance;
import com.atguigu.yuntai.schedule.service.ScheduleService;
import com.atguigu.yuntai.statistics.bean.QActivity;
import com.atguigu.yuntai.statistics.entity.AdsActivityStats;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

/**
 * 调度 前端控制器
 */
@RestController
@RequestMapping("/schedule")
@Api(tags = "调度")
//@CrossOrigin //跨域
@Slf4j
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;


    /**
     * 根据开始时间和活动名称查询所有活动统计
     * @param
     * @return
     */
    @ApiOperation(value = "根据开始时间和活动名称查询所有活动统计")
    @GetMapping("getScheduleList/{page}/{limit}")
    @CrossOrigin
    public Result getScheduleList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(name = "qTaskInstance", value = "查询条件", required = true)
                    QTaskInstance qTaskInstance) {
        Page<TaskInstance> pageParam = new Page<>(page, limit);
        IPage<TaskInstance> pageModel = scheduleService.getScheduleList(pageParam,qTaskInstance);
        return Result.ok(pageModel);
    }

    /**
     * 根据code查询调度详情
     * @param code
     * @return
     */
    @ApiOperation(value = "根据code查询调度详情")
    @GetMapping("getSchedelDetail/{code}")
    @CrossOrigin
    public Result getSchedelDetailById(
            @ApiParam(name = "code", value = "当前页码", required = true)
            @PathVariable BigInteger code){
       return  Result.ok(scheduleService.getSchedelDetailById(code));
    }

    /**
     * 根据调度name查询调度详情
     * @param name
     * @return
     */
    @ApiOperation(value = "根据调度name查询调度详情")
    @GetMapping("getSchedelDetailByName/{name}")
    @CrossOrigin
    public Result getSchedelDetailByName(
            @ApiParam(name = "name", value = "调度名称", required = true)
            @PathVariable String name){
        return  Result.ok(scheduleService.getSchedelDetailByName(name));
    }


}
