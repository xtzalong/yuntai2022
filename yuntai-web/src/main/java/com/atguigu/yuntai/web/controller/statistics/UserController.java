package com.atguigu.yuntai.web.controller.statistics;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.service.UserActionService;
import com.atguigu.yuntai.statistics.service.UserRetentionService;
import com.atguigu.yuntai.statistics.service.UserStatsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户主题controller
 */
@RestController
@RequestMapping("/statistics/user")
@Api(tags = "离线指标")
//@CrossOrigin //跨域
@Slf4j
public class UserController {

    @Autowired
    private UserStatsService userStatsService;

    @Autowired
    private UserActionService userActionService;

    @Autowired
    private UserRetentionService userRetentionService;

    /**
     * 用户行为分析
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "用户行为分析")
    @GetMapping("getUserActionConvert")
    @CrossOrigin
    public Result getUserActionConvert(
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(userActionService.getUserActionConvert(qUnified));
    }


    /**
     * 用户留存率
     *
     * @param dt
     * @return
     */
    @ApiOperation(value = "用户留存率")
    @GetMapping("getUserRetention/{dt}")
    @CrossOrigin
    public Result getUserRetention(
            @ApiParam(name = "dt", value = "统计日期", required = true)
            @PathVariable String dt) {
        return Result.ok(userRetentionService.getUseRetention(dt));
    }

    /**
     * 用户新增活跃以及变动统计
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "用户新增活跃以及变动统计")
    @GetMapping("getUserTotal")
    @CrossOrigin
    public Result getUserTotal(
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(userStatsService.getUser(qUnified));
    }
}
