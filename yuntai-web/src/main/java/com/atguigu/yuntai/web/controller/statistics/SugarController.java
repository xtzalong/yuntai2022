package com.atguigu.yuntai.web.controller.statistics;

import com.atguigu.yuntai.statistics.bean.ReturnCategory;
import com.atguigu.yuntai.statistics.bean.ReturnSpu;
import com.atguigu.yuntai.statistics.entity.KeywordStats;
import com.atguigu.yuntai.statistics.entity.ProductStats;
import com.atguigu.yuntai.statistics.entity.ProvinceStats;
import com.atguigu.yuntai.statistics.entity.VisitorStats;
import com.atguigu.yuntai.statistics.service.SugarServer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 实时统计
 */
@RestController
@RequestMapping("/statistics/sugar")
@Api(tags = "sugar实时统计")
//@CrossOrigin //跨域
@Slf4j
public class SugarController {

    @Autowired
    private SugarServer sugarServer;

    /**
     *获取某一天的总交易额
     * @param date
     * @return
     */
    @ApiOperation(value = "获取某一天的总交易额")
    @RequestMapping("/sugarGmv")
    @CrossOrigin
    public String getGMV(@RequestParam(value = "date", defaultValue = "0") Integer date) {
        if (date == 0) {
            date = now();
        }
        BigDecimal gmv = sugarServer.getGMV(date);
        String json = "{   \"status\": 0,  \"data\":" + gmv + "}";
        return json;
    }

    /**
     * 统计某天不同SPU商品交易额排名
     * @param date
     * @param limit
     * @return
     */
    @ApiOperation(value = "统计某天不同SPU商品交易额排名")
    @RequestMapping("/spuSugar")
    @CrossOrigin
    public String getProductStatsGroupBySpu(
            @RequestParam(value = "date", defaultValue = "0") Integer date,
            @RequestParam(value = "limit", defaultValue = "10") int limit) {
        if (date == 0) date = now();
        List<ReturnSpu> statsList
                = sugarServer.getProductStatsGroupBySpu(date, limit);
        //设置表头
        StringBuilder jsonBuilder =
                new StringBuilder(" " +
                        "{\"status\":0,\"data\":{\"columns\":[" +
                        "{\"name\":\"商品名称\",\"id\":\"spu_name\"}," +
                        "{\"name\":\"交易额\",\"id\":\"order_amount\"}," +
                        "{\"name\":\"订单数\",\"id\":\"order_ct\"}]," +
                        "\"rows\":[");
        //循环拼接表体
        for (int i = 0; i < statsList.size(); i++) {
            ReturnSpu productStats = statsList.get(i);
            if (i >= 1) {
                jsonBuilder.append(",");
            }
            jsonBuilder.append("{\"spu_name\":\"" + productStats.getSpuName() + "\"," +
                    "\"order_amount\":" + productStats.getOrderAmount() + "," +
                    "\"order_ct\":" + productStats.getOrderCt() + "}");

        }
        jsonBuilder.append("]}}");
        return jsonBuilder.toString();
    }

    /*
{
    "status": 0,
    "data": [
        {
            "name": "数码类",
            "value": 371570
        },
        {
            "name": "日用品",
            "value": 296016
        }
    ]
}
 */
    @ApiOperation(value = "统计某天不同类别商品交易额排名")
    @RequestMapping("/category3Sugar")
    @CrossOrigin
    public String getProductStatsGroupByCategory3(
            @RequestParam(value = "date", defaultValue = "0") Integer date,
            @RequestParam(value = "limit", defaultValue = "4") int limit) {
        if (date == 0) {
            date = now();
        }
        List<ReturnCategory> statsList
                = sugarServer.getProductStatsGroupByCategory3(date, limit);

        StringBuilder dataJson = new StringBuilder("{  \"status\": 0, \"data\": [");
        int i = 0;
        for (ReturnCategory productStats : statsList) {
            if (i++ > 0) {
                dataJson.append(",");
            }
            ;
            dataJson.append("{\"name\":\"")
                    .append(productStats.getCategory3Name()).append("\",");
            dataJson.append("\"value\":")
                    .append(productStats.getOrderAmount()).append("}");
        }
        dataJson.append("]}");
        return dataJson.toString();
    }


    /*
    {
     "status": 0,
     "data": {
         "categories": [
             "三星","vivo","oppo"
         ],
         "series": [
             {
                 "data": [ 406333, 709174, 681971
                 ]
             }
         ]
     }
    }
*/
    @ApiOperation(value = "统计某天不同品牌商品交易额排名")
    @RequestMapping("/trademarkSugar")
    @CrossOrigin
    public String getProductStatsByTrademark(
            @RequestParam(value = "date", defaultValue = "0") Integer date,
            @RequestParam(value = "limit", defaultValue = "20") int limit) {
        if (date == 0) {
            date = now();
        }
        List<ProductStats> productStatsByTrademarkList
                = sugarServer.getProductStatsByTrademark(date, limit);
        List<String> tradeMarkList = new ArrayList<>();
        List<BigDecimal> amountList = new ArrayList<>();
        for (ProductStats productStats : productStatsByTrademarkList) {
            tradeMarkList.add(productStats.getTmName());
            amountList.add(productStats.getOrderAmount());
        }
        String json = "{\"status\":0,\"data\":{" + "\"categories\":" +
                "[\"" + StringUtils.join(tradeMarkList, "\",\"") + "\"],\"series\":[" +
                "{\"data\":[" + StringUtils.join(amountList, ",") + "]}]}}";
        return json;
    }

    @ApiOperation(value = "地区")
    @RequestMapping("/provinceSugar")
    @CrossOrigin
    public String getProvinceStats(@RequestParam(value = "date", defaultValue = "0") Integer date) {
        if (date == 0) {
            date = now();
        }

        StringBuilder jsonBuilder = new StringBuilder("{\"status\":0,\"data\":{\"mapData\":[");
        List<ProvinceStats> provinceStatsList = sugarServer.getProvinceStats(date);
        if (provinceStatsList.size() == 0) {
            //    jsonBuilder.append(  "{\"name\":\"北京\",\"value\":0.00}");
        }
        for (int i = 0; i < provinceStatsList.size(); i++) {
            if (i >= 1) {
                jsonBuilder.append(",");
            }
            ProvinceStats provinceStats = provinceStatsList.get(i);
            jsonBuilder.append("{\"name\":\"" + provinceStats.getProvinceName() + "\",\"value\":" + provinceStats.getOrderAmount() + " }");

        }
        jsonBuilder.append("]}}");
        return jsonBuilder.toString();
    }

    @ApiOperation(value = "新老用户对比")
    @RequestMapping("/visitorSugar")
    @CrossOrigin
    public String getVisitorStatsByNewFlag(@RequestParam(value = "date", defaultValue = "0") Integer date) {
        if (date == 0) date = now();
        List<VisitorStats> visitorStatsByNewFlag = sugarServer.getVisitorStatsByNewFlag(date);
        VisitorStats newVisitorStats = new VisitorStats();
        VisitorStats oldVisitorStats = new VisitorStats();
        //循环把数据赋给新访客统计对象和老访客统计对象
        for (VisitorStats visitorStats : visitorStatsByNewFlag) {
            if (visitorStats.getIsNew().equals("1")) {
                newVisitorStats = visitorStats;
            } else {
                oldVisitorStats = visitorStats;
            }
        }
        //把数据拼接入字符串
        String json = "{\"status\":0,\"data\":{\"combineNum\":1,\"columns\":" +
                "[{\"name\":\"类别\",\"id\":\"type\"}," +
                "{\"name\":\"新用户\",\"id\":\"new\"}," +
                "{\"name\":\"老用户\",\"id\":\"old\"}]," +
                "\"rows\":" +
                "[{\"type\":\"用户数(人)\"," +
                "\"new\": " + newVisitorStats.getUvCt() + "," +
                "\"old\":" + oldVisitorStats.getUvCt() + "}," +
                "{\"type\":\"总访问页面(次)\"," +
                "\"new\":" + newVisitorStats.getPvCt() + "," +
                "\"old\":" + oldVisitorStats.getPvCt() + "}," +
                "{\"type\":\"跳出率(%)\"," +
                "\"new\":" + newVisitorStats.getUjRate() + "," +
                "\"old\":" + oldVisitorStats.getUjRate() + "}," +
                "{\"type\":\"平均在线时长(秒)\"," +
                "\"new\":" + newVisitorStats.getDurPerSv() + "," +
                "\"old\":" + oldVisitorStats.getDurPerSv() + "}," +
                "{\"type\":\"平均访问页面数(人次)\"," +
                "\"new\":" + newVisitorStats.getPvPerSv() + "," +
                "\"old\":" + oldVisitorStats.getPvPerSv()
                + "}]}}";
        return json;
    }

    @ApiOperation(value = "用户访问分时")
    @RequestMapping("/hrSugar")
    @CrossOrigin
    public String getMidStatsGroupbyHourNewFlag(@RequestParam(value = "date", defaultValue = "0") Integer date) {
        if (date == 0) date = now();
        List<VisitorStats> visitorStatsHrList
                = sugarServer.getVisitorStatsByHour(date);

        //构建24位数组
        VisitorStats[] visitorStatsArr = new VisitorStats[24];

        //把对应小时的位置赋值
        for (VisitorStats visitorStats : visitorStatsHrList) {
            visitorStatsArr[visitorStats.getHr()] = visitorStats;
        }
        List<String> hrList = new ArrayList<>();
        List<Long> uvList = new ArrayList<>();
        List<Long> pvList = new ArrayList<>();
        List<Long> newMidList = new ArrayList<>();

        //循环出固定的0-23个小时  从结果map中查询对应的值
        for (int hr = 0; hr <= 23; hr++) {
            VisitorStats visitorStats = visitorStatsArr[hr];
            if (visitorStats != null) {
                uvList.add(visitorStats.getUvCt());
                pvList.add(visitorStats.getPvCt());
                newMidList.add(visitorStats.getNewUv());
            } else { //该小时没有流量补零
                uvList.add(0L);
                pvList.add(0L);
                newMidList.add(0L);
            }
            //小时数不足两位补零
            hrList.add(String.format("%02d", hr));
        }
        //拼接字符串
        String json = "{\"status\":0,\"data\":{" + "\"categories\":" +
                "[\"" + StringUtils.join(hrList, "\",\"") + "\"],\"series\":[" +
                "{\"name\":\"uv\",\"data\":[" + StringUtils.join(uvList, ",") + "]}," +
                "{\"name\":\"pv\",\"data\":[" + StringUtils.join(pvList, ",") + "]}," +
                "{\"name\":\"新用户\",\"data\":[" + StringUtils.join(newMidList, ",") + "]}]}}";
        return json;
    }

    @ApiOperation(value = "热门词汇")
    @RequestMapping("/keywordSugar")
    @CrossOrigin
    public String getKeywordStats(@RequestParam(value = "date", defaultValue = "0") Integer date,
                                  @RequestParam(value = "limit", defaultValue = "20") int limit) {
        if (date == 0) {
            date = now();
        }
        //查询数据
        List<KeywordStats> keywordStatsList
                = sugarServer.getKeywordStats(date, limit);
        StringBuilder jsonSb = new StringBuilder("{\"status\":0,\"msg\":\"\",\"data\":[");
        //循环拼接字符串
        for (int i = 0; i < keywordStatsList.size(); i++) {
            KeywordStats keywordStats = keywordStatsList.get(i);
            if (i >= 1) {
                jsonSb.append(",");
            }
            jsonSb.append("{\"name\":\"" + keywordStats.getKeyword() + "\"," +
                    "\"value\":" + keywordStats.getCt() + "}");
        }
        jsonSb.append("]}");
        return jsonSb.toString();
    }

    private int now() {
        String yyyyMMdd = DateFormatUtils.format(new Date(), "yyyyMMdd");
        return Integer.valueOf(yyyyMMdd);
    }


}
