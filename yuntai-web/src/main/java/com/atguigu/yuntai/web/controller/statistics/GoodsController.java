package com.atguigu.yuntai.web.controller.statistics;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.statistics.bean.QUnified;
import com.atguigu.yuntai.statistics.entity.AdsTradeStatsByTm;
import com.atguigu.yuntai.statistics.service.RepeatPurchaseService;
import com.atguigu.yuntai.statistics.service.SkuCartNumService;
import com.atguigu.yuntai.statistics.service.TradeStatsCateService;
import com.atguigu.yuntai.statistics.service.TradeStatsTmService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品统计
 */
@RestController
@RequestMapping("/statistics/goods")
@Api(tags = "商品主题指标")
//@CrossOrigin //跨域
@Slf4j
public class GoodsController {

    @Autowired
    private RepeatPurchaseService repeatPurchaseService;

    @Autowired
    private TradeStatsTmService tradeStatsTmService;

    @Autowired
    private TradeStatsCateService tradeStatsCateService;

    @Autowired
    private SkuCartNumService skuCartNumService;

    /**
     * 最近7/30日各品牌复购率
     *
     * @param showNum
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "最近7/30日各品牌复购率")
    @GetMapping("getTmRepeat/{showNum}")
    @CrossOrigin
    public Result getTmRepeat(
            @ApiParam(name = "showNum", value = "当前页码", required = true)
            @PathVariable int showNum,
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(repeatPurchaseService.getTmRepeat(qUnified, showNum));
    }


    /**
     * 各品牌商品交易统计
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "各品牌商品交易统计")
    @GetMapping("getTmTradeStats/{page}/{limit}")
    @CrossOrigin
    public Result getTmTradeStats(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        Page<AdsTradeStatsByTm> pageParam = new Page<>(page, limit);
        IPage<AdsTradeStatsByTm> pageModel = tradeStatsTmService.selectPage(pageParam, qUnified);
        return Result.ok(pageModel);
    }

    /**
     * 各品类商品交易统计
     *
     * @param qUnified
     * @return
     */
    @ApiOperation(value = "各品类商品交易统计")
    @GetMapping("getCateTradeStats")
    @CrossOrigin
    public Result getCateTradeStats(
            @ApiParam(name = "qUnified", value = "查询条件", required = true)
                    QUnified qUnified) {
        return Result.ok(tradeStatsCateService.getCateTradeStats(qUnified));
    }

    /**
     * 各分类商品购物车存量Top10:查询一级分类
     *
     * @param curDate
     * @return
     */
    @ApiOperation(value = "各分类商品购物车存量Top10:查询一级分类")
    @GetMapping("getCategory1/{curDate}")
    @CrossOrigin
    public Result getCategory1(
            @ApiParam(name = "curDate", value = "查询条件", required = true)
            @PathVariable String curDate) {
        return Result.ok(skuCartNumService.getCategory1(curDate));
    }

    /**
     * 各分类商品购物车存量Top10:查询二级分类
     *
     * @param curDate
     * @param category1Id
     * @return
     */
    @ApiOperation(value = "各分类商品购物车存量Top10:查询二级分类")
    @GetMapping("getCategory2/{curDate}/{category1Id}")
    @CrossOrigin
    public Result getCategory2(
            @ApiParam(name = "curDate", value = "查询条件", required = true)
            @PathVariable String curDate,
            @ApiParam(name = "category1Id", value = "查询条件", required = true)
            @PathVariable String category1Id) {
        return Result.ok(skuCartNumService.getCategory2(curDate, category1Id));
    }

    /**
     * 各分类商品购物车存量Top10:查询三级分类
     *
     * @param curDate
     * @param category1Id
     * @param category2Id
     * @return
     */
    @ApiOperation(value = "各分类商品购物车存量Top10:查询三级分类")
    @GetMapping("getCategory3/{curDate}/{category1Id}/{category2Id}")
    @CrossOrigin
    public Result getCategory3(
            @ApiParam(name = "curDate", value = "查询条件", required = true)
            @PathVariable String curDate,
            @ApiParam(name = "category1Id", value = "查询条件", required = true)
            @PathVariable String category1Id,
            @ApiParam(name = "category2Id", value = "查询条件", required = true)
            @PathVariable String category2Id) {
        return Result.ok(skuCartNumService.getCategory3(curDate, category1Id, category2Id));
    }

    /**
     * 各分类商品购物车存量Top10
     *
     * @param curDate
     * @param category1Id
     * @param category2Id
     * @param category3Id
     * @return
     */
    @ApiOperation(value = "各分类商品购物车存量Top10")
    @GetMapping("getTmTopNData/{curDate}/{category1Id}/{category2Id}/{category3Id}")
    @CrossOrigin
    public Result getTmTopNData(
            @ApiParam(name = "curDate", value = "查询条件", required = true)
            @PathVariable String curDate,
            @ApiParam(name = "category1Id", value = "查询条件", required = true)
            @PathVariable String category1Id,
            @ApiParam(name = "category2Id", value = "查询条件", required = true)
            @PathVariable String category2Id,
            @ApiParam(name = "category3Id", value = "查询条件", required = true)
            @PathVariable String category3Id) {
        return Result.ok(skuCartNumService.getTmTopNData(curDate, category1Id, category2Id, category3Id));
    }

}
