package com.atguigu.yuntai.web.controller.acl;

import com.atguigu.yuntai.acl.bean.AdminQueryBean;
import com.atguigu.yuntai.acl.entity.Admin;
import com.atguigu.yuntai.acl.service.AdminService;
import com.atguigu.yuntai.acl.service.RoleService;
import com.atguigu.yuntai.common.util.MD5;
import com.atguigu.yuntai.common.util.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 用户管理 前端控制器
 */
@RestController
@RequestMapping("/admin/acl/user")
@Api(tags = "用户管理")
//@CrossOrigin //跨域
@Slf4j
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private RoleService roleService;

    // @PreAuthorize("hasAuthority('user.list')")

    /**
     * 获取管理用户分页列表
     *
     * @param page
     * @param limit
     * @param userQueryBean
     * @return
     */
    @ApiOperation(value = "获取管理用户分页列表")
    @GetMapping("{page}/{limit}")
    public Result index(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(name = "userQueryBean", value = "查询对象", required = false)
                    AdminQueryBean userQueryBean) {
        Page<Admin> pageParam = new Page<>(page, limit);
        IPage<Admin> pageModel = adminService.selectPage(pageParam, userQueryBean);
        return Result.ok(pageModel);
    }

    /**
     * 获取管理用户
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "获取管理用户")
    @GetMapping("get/{id}")
    public Result get(@PathVariable Long id) {
        Admin user = adminService.getById(id);
        return Result.ok(user);
    }

    /**
     * 新增管理用户
     *
     * @param user
     * @return
     */
    @ApiOperation(value = "新增管理用户")
    @PostMapping("save")
    public Result save(@RequestBody Admin user) {
        user.setPassword(MD5.encrypt(user.getPassword()));
        adminService.save(user);
        return Result.ok();
    }

    /**
     * 修改管理用户
     *
     * @param user
     * @return
     */
    @ApiOperation(value = "修改管理用户")
    @PutMapping("update")
    public Result updateById(@RequestBody Admin user) {
        adminService.updateById(user);
        return Result.ok();
    }

    /**
     * 删除管理用户
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除管理用户")
    @DeleteMapping("remove/{id}")
    public Result remove(@PathVariable Long id) {
        adminService.removeById(id);
        return Result.ok();
    }

    /**
     * 根据id列表删除管理用户
     *
     * @param idList
     * @return
     */
    @ApiOperation(value = "根据id列表删除管理用户")
    @DeleteMapping("batchRemove")
    public Result batchRemove(@RequestBody List<Long> idList) {
        adminService.removeByIds(idList);
        return Result.ok();
    }

    /**
     * 根据用户获取角色数据
     *
     * @param adminId
     * @return
     */
    @ApiOperation(value = "根据用户获取角色数据")
    @GetMapping("/toAssign/{adminId}")
    public Result toAssign(@PathVariable Long adminId) {
        Map<String, Object> roleMap = roleService.findRoleByUserId(adminId);
        return Result.ok(roleMap);
    }

    /**
     * 根据用户分配角色
     *
     * @param adminId
     * @param roleId
     * @return
     */
    @ApiOperation(value = "根据用户分配角色")
    @PostMapping("/doAssign")
    public Result doAssign(@RequestParam Long adminId, @RequestParam Long[] roleId) {
        roleService.saveUserRoleRealtionShip(adminId, roleId);
        return Result.ok();
    }


    /**
     * 获取管理用户
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "获取管理用户")
    @GetMapping("getNameById/{id}")
    public String getNameById(@PathVariable Long id) {
        Admin user = adminService.getById(id);
        return user.getName();
    }
}

