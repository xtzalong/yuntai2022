package com.atguigu.yuntai.web.controller.monitor;

import com.atguigu.yuntai.common.util.Result;
import com.atguigu.yuntai.monitor.bean.AlertMsgRowVO;
import com.atguigu.yuntai.monitor.service.AlertMsgService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 报警 前端控制器
 */
@RestController
@RequestMapping("/monitor/alert")
@Api(tags = "报警")
//@CrossOrigin //跨域
@Slf4j
public class AlertController {

    @Autowired
    AlertMsgService alertMsgService;

    /**
     * 根据触发id来查看报警信息
     *
     * @param triggerId
     * @return
     */
    @ApiOperation(value = "根据触发id来查看报警信息")
    @GetMapping("/listAlertMsgByTriggerId/{triggerId}")
    @CrossOrigin
    public Result listAlertMsgByCollectionId(
            @ApiParam(name = "triggerId", value = "触发Id", required = true)
            @PathVariable Long triggerId) {
        return Result.ok(alertMsgService.listAlertMsgByTriggerId(triggerId));
    }

    /**
     * 批量将报警信息标记为已读
     * @param idList
     * @return
     */
    @ApiOperation(value = "批量将报警信息标记为已读")
    @PutMapping("/read/")
    @CrossOrigin
    public Result read(
            @ApiParam(name = "idList", value = "json字符串", required = true)
            @RequestBody List<Long> idList) {
        alertMsgService.read(idList);
        return Result.ok();
    }

    /**
     * 单个将报警信息标记为已读
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "单个将报警信息标记为已读")
    @PutMapping("updateSend/{id}")
    public Result remove(@PathVariable Long id) {
        alertMsgService.updateSendById(id);
        return Result.ok();
    }

    /**
     * 获取未读报警信息的条数
     *
     * @return
     */
    @ApiOperation(value = "获取未读报警信息的条数")
    @GetMapping("/getUnreadNum")
    @CrossOrigin
    public Result getUnreadNum() {
        return Result.ok(alertMsgService.countUnread());
    }

    /**
     * 获取报警信息的视图
     *
     * @param page
     * @param limit
     * @return
     */
    @ApiOperation(value = "获取报警信息的视图")
    @GetMapping("getAlertMsgVO/{page}/{limit}")
    @CrossOrigin
    public Result getAlertMsgVO(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit) {
        Page<AlertMsgRowVO> pageParam = new Page<>(page, limit);
        IPage<AlertMsgRowVO> pageModel = alertMsgService.getAlertMsgVO(pageParam);
        return Result.ok(pageModel);
    }
}
