package com.atguigu.yuntai.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
//@EnableScheduling
//@EnableSwagger2
@ComponentScan(basePackages = {"com.atguigu.yuntai"})
//@EnableNeo4jRepositories(basePackages = {"com.atguigu.yuntai.lineage.dao"})
@MapperScan(basePackages = {"com.atguigu.yuntai.schedule.mapper"})
//@Configuration("com.atguigu.yuntai.monitor.config")
public class YunTaiWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(YunTaiWebApplication.class, args);

    }

}
