/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-yuntai
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : aliyun102:3306
 Source Schema         : yuntai_monitor

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 01/08/2022 15:59:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alert_msg
-- ----------------------------
DROP TABLE IF EXISTS `alert_msg`;
CREATE TABLE `alert_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `trigger_log_id` bigint(20) NOT NULL COMMENT '触发记录的id',
  `collection_id` bigint(11) DEFAULT NULL,
  `rule_info_compute_id` bigint(20) DEFAULT NULL COMMENT '规则compute的id',
  `msg` text NOT NULL COMMENT '报警消息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `if_send` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否发送',
  `if_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已读',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of alert_msg
-- ----------------------------
BEGIN;
INSERT INTO `alert_msg` VALUES (1, 1, 12, NULL, '执行异常java.lang.Integer cannot be cast to java.lang.String', '2022-07-16 17:54:07', '2022-07-16 17:54:07', 0, 0);
INSERT INTO `alert_msg` VALUES (2, 2, 12, NULL, '执行异常java.lang.Integer cannot be cast to java.lang.String', '2022-07-16 17:55:41', '2022-07-16 17:55:41', 0, 0);
INSERT INTO `alert_msg` VALUES (3, 3, 12, NULL, '执行异常java.lang.Integer cannot be cast to java.lang.String', '2022-07-16 17:58:29', '2022-07-16 17:58:29', 0, 0);
INSERT INTO `alert_msg` VALUES (4, 4, 16, NULL, '执行异常select count(1) from gmall.ods_refund_payment_inc where dt=\'2022-07-15\'执行失败', '2022-07-16 18:16:44', '2022-07-16 18:16:44', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for rule_collection
-- ----------------------------
DROP TABLE IF EXISTS `rule_collection`;
CREATE TABLE `rule_collection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键，自增id',
  `hive_database` varchar(255) DEFAULT NULL COMMENT 'hive数据库名',
  `hive_table_name` varchar(255) DEFAULT NULL COMMENT 'hive表名',
  `name` varchar(255) NOT NULL COMMENT '规则集合名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `if_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rule_collection
-- ----------------------------
BEGIN;
INSERT INTO `rule_collection` VALUES (12, 'gmall', 'ods_refund_payment_inc', '空值率', '2022-07-12 13:52:53', 0);
INSERT INTO `rule_collection` VALUES (16, 'gmall', 'ods_refund_payment_inc', '3333', '2022-07-14 17:15:50', 0);
INSERT INTO `rule_collection` VALUES (17, 'gmall', 'ods_activity_info_full', '111', '2022-07-27 18:05:46', 0);
INSERT INTO `rule_collection` VALUES (18, 'gmall', 'ods_activity_info_full', '222', '2022-07-27 18:06:17', 0);
INSERT INTO `rule_collection` VALUES (19, 'gmall', 'ods_activity_info_full', '33', '2022-07-29 20:39:50', 0);
INSERT INTO `rule_collection` VALUES (20, 'gmall', 'ods_activity_info_full', '444', '2022-07-29 22:39:16', 0);
INSERT INTO `rule_collection` VALUES (21, 'gmall', 'ods_activity_info_full', '555', '2022-08-01 10:43:14', 0);
COMMIT;

-- ----------------------------
-- Table structure for rule_info
-- ----------------------------
DROP TABLE IF EXISTS `rule_info`;
CREATE TABLE `rule_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `rule_collection_id` bigint(20) NOT NULL COMMENT '目标表的id(联表查询用)',
  `name` varchar(255) NOT NULL COMMENT '规则的名称',
  `analyse_type` varchar(255) NOT NULL COMMENT '分析的类型(数值分析，对比分析)',
  `rule_level` varchar(255) DEFAULT NULL COMMENT '规则类型(表级,字段级)',
  `base_partition_def` json DEFAULT NULL COMMENT '前置分区表达式',
  `check_partition_def` json NOT NULL COMMENT '后置分区表达式',
  `fields_name` text COMMENT '字段名，允许多个，逗号分隔',
  `calculate_type` varchar(255) DEFAULT NULL COMMENT '计算类型(总行数,增长率,控制率,命中率等)',
  `fixed_compare_way` varchar(255) DEFAULT NULL COMMENT '固定值的比较方式 > < >= <= 等',
  `fixed_value` decimal(19,6) DEFAULT NULL COMMENT '固定值',
  `swing_upper` decimal(19,6) DEFAULT NULL COMMENT '波动上限',
  `swing_lower` decimal(19,6) DEFAULT NULL COMMENT '波动下限',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `if_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rule_info
-- ----------------------------
BEGIN;
INSERT INTO `rule_info` VALUES (1, 12, '111', '数值', 'table', '{\"dsl\": \"111\", \"name\": \"dt\", \"type\": \"String\"}', '{\"dsl\": \"111\", \"name\": \"dt\", \"type\": \"String\"}', NULL, 'countAll', '>=', 111.000000, 11.000000, 111.000000, '2022-07-16 11:02:56', '2022-07-16 11:02:56', 0);
INSERT INTO `rule_info` VALUES (2, 12, '222', '数值', 'fields', '{\"dsl\": \"222\", \"name\": \"dt\", \"type\": \"String\"}', '{\"dsl\": \"222\", \"name\": \"dt\", \"type\": \"String\"}', 'type', 'sum', '>=', 22.000000, 222.000000, 222.000000, '2022-07-16 11:04:18', '2022-07-16 11:04:18', 0);
INSERT INTO `rule_info` VALUES (5, 16, '总行数', '数值', 'table', '{\"dsl\": \"date(\\\"yyyy-MM-dd\\\",{dt-1.day})\", \"name\": \"dt\", \"type\": \"String\"}', '{\"dsl\": \"date(\\\"yyyy-MM-dd\\\",{dt})\", \"name\": \"dt\", \"type\": \"String\"}', NULL, 'countAll', '<=', 11.000000, 5.000000, 2.000000, '2022-07-16 18:16:33', '2022-07-16 18:16:33', 0);
INSERT INTO `rule_info` VALUES (6, 12, '33333', '数值', 'table', '{\"dsl\": \"4444\", \"name\": \"\", \"type\": \"String\"}', '{\"dsl\": \"44444\", \"name\": \"\", \"type\": \"String\"}', NULL, 'countAll', '>', 544545.000000, 454.000000, 4545.000000, '2022-07-26 11:16:22', '2022-07-26 11:16:22', 0);
INSERT INTO `rule_info` VALUES (7, 12, '44444', '', 'table', '{\"dsl\": \"4444\", \"name\": \"\", \"type\": \"String\"}', '{\"dsl\": \"4444\", \"name\": \"dt\", \"type\": \"String\"}', NULL, 'countAll', '', NULL, NULL, NULL, '2022-07-26 11:26:39', '2022-07-26 11:26:39', 0);
INSERT INTO `rule_info` VALUES (8, 17, '222', '数值', 'table', '{\"dsl\": \"222\", \"name\": \"dt\", \"type\": \"String\"}', '{\"dsl\": \"222\", \"name\": \"dt\", \"type\": \"String\"}', NULL, 'countAll', '>', 222.000000, 333.000000, 4444.000000, '2022-07-27 18:06:11', '2022-07-27 18:06:11', 0);
COMMIT;

-- ----------------------------
-- Table structure for rule_info_compute
-- ----------------------------
DROP TABLE IF EXISTS `rule_info_compute`;
CREATE TABLE `rule_info_compute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `rule_info_id` bigint(20) NOT NULL COMMENT '规则信息的id',
  `trigger_id` bigint(20) NOT NULL COMMENT '触发日志的id',
  `base_result` decimal(19,6) DEFAULT NULL COMMENT '基准分区的计算结果',
  `check_result` decimal(19,6) DEFAULT NULL COMMENT '受检分区的计算结果',
  `if_alert` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否触发了报警',
  `rule_collection_id` bigint(20) NOT NULL COMMENT '集合id',
  `name` varchar(255) NOT NULL COMMENT '规则名称',
  `analyse_type` varchar(255) NOT NULL COMMENT '分析的类型',
  `rule_level` varchar(255) DEFAULT NULL COMMENT '规则类型(表级,字段级)',
  `base_partition_def` json DEFAULT NULL COMMENT '基准分区表达式',
  `check_partition_def` json DEFAULT NULL COMMENT '后置分区表达式',
  `fields_name` text COMMENT '字段名，允许多个，逗号分隔',
  `calculate_type` varchar(255) DEFAULT NULL COMMENT '计算类型(总行数,增长率,控制率,命中率)',
  `fixed_compare_way` varchar(255) DEFAULT NULL COMMENT ' 固定值的比较方式 > < >= <=等',
  `fixed_value` decimal(19,6) DEFAULT NULL COMMENT '固定值',
  `swing_upper` decimal(19,6) DEFAULT NULL COMMENT '波动上限',
  `swing_lower` decimal(19,6) DEFAULT NULL COMMENT '波动下限',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `if_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of rule_info_compute
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for trigger_log
-- ----------------------------
DROP TABLE IF EXISTS `trigger_log`;
CREATE TABLE `trigger_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `collection_id` bigint(20) NOT NULL COMMENT '集合id',
  `who` varchar(255) NOT NULL COMMENT '触发者',
  `if_alert` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '是否发生了报警',
  `trigger_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '触发时间',
  `statue` varchar(255) NOT NULL DEFAULT 'created' COMMENT '任务的状态',
  `computed_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of trigger_log
-- ----------------------------
BEGIN;
INSERT INTO `trigger_log` VALUES (1, 12, 'admin', 1, '2022-07-16 17:54:07', 'created', '2022-07-16 17:54:07');
INSERT INTO `trigger_log` VALUES (2, 12, 'admin', 1, '2022-07-16 17:55:41', 'created', '2022-07-16 17:55:41');
INSERT INTO `trigger_log` VALUES (3, 12, 'admin', 1, '2022-07-16 17:58:09', 'created', '2022-07-16 17:58:29');
INSERT INTO `trigger_log` VALUES (4, 16, 'admin', 1, '2022-07-16 18:16:42', 'created', '2022-07-16 18:16:44');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
