package com.atguigu.yuntai.acl.service;

import com.atguigu.yuntai.acl.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色权限服务接口
 */
public interface RolePermissionService extends IService<RolePermission> {


}
