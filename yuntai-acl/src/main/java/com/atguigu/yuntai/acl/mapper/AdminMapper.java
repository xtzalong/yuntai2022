package com.atguigu.yuntai.acl.mapper;

import com.atguigu.yuntai.acl.bean.AdminQueryBean;
import com.atguigu.yuntai.acl.entity.Admin;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 用户Mpper接口
 */
@DS("mysql")
public interface AdminMapper extends BaseMapper<Admin> {


    @Select("<script> " +
            "select id,username,password,name,phone,warehouse_id,is_deleted,create_time,update_time " +
            "from admin " +
            " <where> is_deleted = 0" +
            "            <if test=\"vo.username != null and vo.username != ''\">\n" +
            "                and username like CONCAT('%',#{vo.username},'%')\n" +
            "            </if>\n" +
            "            <if test=\"vo.name != null and vo.name != ''\">\n" +
            "                and name like CONCAT('%',#{vo.name},'%')\n" +
            "            </if>\n" +
            "        </where>" +
            "        order by id desc" +
            "</script> ")
    IPage<Admin> selectPage(Page<Admin> page, @Param("vo") AdminQueryBean userQueryBean);
}
