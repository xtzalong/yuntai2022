package com.atguigu.yuntai.acl.service;


import com.atguigu.yuntai.acl.entity.AdminRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户角色服务接口
 */
public interface AdminRoleService extends IService<AdminRole> {

    /**
     * 根据用户id获取角色信息
     *
     * @param adminId
     * @return
     */
    String getRoleNameByAdminId(Long adminId);
}
