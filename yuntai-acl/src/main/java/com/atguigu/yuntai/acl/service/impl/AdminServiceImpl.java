package com.atguigu.yuntai.acl.service.impl;


import com.atguigu.yuntai.acl.bean.AdminQueryBean;
import com.atguigu.yuntai.acl.entity.Admin;
import com.atguigu.yuntai.acl.mapper.AdminMapper;
import com.atguigu.yuntai.acl.service.AdminRoleService;
import com.atguigu.yuntai.acl.service.AdminService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户角色服务实现类
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private AdminRoleService userRoleService;

    @Override
    public IPage<Admin> selectPage(Page<Admin> pageParam, AdminQueryBean userQueryBean) {
        IPage<Admin> page = adminMapper.selectPage(pageParam, userQueryBean);
        page.getRecords().stream().forEach(item -> {
            item.setRoleName(userRoleService.getRoleNameByAdminId(item.getId()));
        });
        return page;
    }

    @Override
    public Admin selectByUsername(String username) {
        return adminMapper.selectOne(new QueryWrapper<Admin>().eq("username", username));
    }
}
