package com.atguigu.yuntai.acl.service;


import com.atguigu.yuntai.acl.bean.RoleQueryBean;
import com.atguigu.yuntai.acl.entity.Role;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 角色服务接口
 */
public interface RoleService extends IService<Role> {

    /**
     * 讲师分页列表
     *
     * @param roleQueryBean
     * @return
     */
    IPage<Role> selectPage(Page<Role> pageParam, RoleQueryBean roleQueryBean);

    /**
     * 分配角色
     *
     * @param adminId
     * @param roleIds
     */
    void saveUserRoleRealtionShip(Long adminId, Long[] roleIds);

    /**
     * 根据用户获取角色数据
     *
     * @param adminId
     * @return
     */
    Map<String, Object> findRoleByUserId(Long adminId);

    /**
     * 根据用户id获取角色列表
     *
     * @param adminId
     * @return
     */
    List<Role> selectRoleByUserId(Long adminId);
}
