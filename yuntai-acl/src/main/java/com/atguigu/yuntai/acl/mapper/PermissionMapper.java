package com.atguigu.yuntai.acl.mapper;

import com.atguigu.yuntai.acl.entity.Permission;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 权限Mpper接口
 */
@DS("mysql")
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
     * 获取用户菜单权限
     *
     * @param adminId
     * @return
     */
    @Select("select p.id,p.pid,p.name,p.type,p.code,p.to_code,p.status from admin_role ur\n" +
            "        inner join role_permission rp on rp.role_id = ur.role_id\n" +
            "        inner join permission p on p.id = rp.permission_id\n" +
            "        where ur.admin_id = #{adminId}\n" +
            "        and ur.is_deleted = 0\n" +
            "        and rp.is_deleted = 0\n" +
            "        and p.is_deleted = 0")
    List<Permission> selectPermissionByUserId(@Param("adminId") Long adminId);

    /**
     * 获取用户操作权限
     *
     * @param adminId
     * @return
     */
    @Select("select\n" +
            "        p.code\n" +
            "        from admin_role ur\n" +
            "        inner join role_permission rp on rp.role_id = ur.role_id\n" +
            "        inner join permission p on p.id = rp.permission_id\n" +
            "        where ur.admin_id = #{adminId}\n" +
            "        and p.type = 2\n" +
            "        and ur.is_deleted = 0\n" +
            "        and rp.is_deleted = 0\n" +
            "        and p.is_deleted = 0")
    List<String> selectPermissionValueByUserId(@Param("adminId") Long adminId);

    /**
     * 获取全部操作权限
     *
     * @return
     */
    @Select("select\n" +
            "        code\n" +
            "        from permission\n" +
            "        where type = 2\n" +
            "        and is_deleted = 0")
    List<String> selectAllPermissionValue();
}
