package com.atguigu.yuntai.monitor.bean;

import com.atguigu.yuntai.monitor.entity.TriggerLog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>触发记录,供前端展示的</p>
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TriggerLogRowVO extends TriggerLog {

    private String hiveDatabase;

    private String hiveTableName;

    private String name;
}
