package com.atguigu.yuntai.monitor.bean;

import lombok.Data;

/**
 * <p>分区表达式定义和(dsl相关)</p>
 *
 **/
@Data
public class PartitionDef {
    /**
     * 字段的名称
     */
    private String name;

    /**
     * 字段的dsl表达式
     */
    private String dsl;

    /**
     * 字段的类型
     */
    private String type;
}
