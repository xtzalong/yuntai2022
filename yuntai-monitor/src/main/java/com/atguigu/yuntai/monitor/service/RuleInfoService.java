package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.entity.RuleInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 规则服务接口
 */
public interface RuleInfoService extends IService<RuleInfo> {

    /**
     * 根据规则集合id查询未删除的规则
     * @param collectionId
     * @return
     */
    List<RuleInfo> listAllRulesByCollectionId(Long collectionId);

    /**
     * 根据规则集合id删除规则
     * @param collectionId
     */
    void deleteRuleInfoByCollectionId(Long collectionId);

    /**
     * 跟新或修改规则信息
     * @param ruleInfo
     */
    void saveOrUpdateOneRuleInfo(RuleInfo ruleInfo);

    /**
     * 根据规则id查询规则详情
     * @param ruleInfoId
     * @return
     */
    RuleInfo getOneRuleInfoById(Long ruleInfoId);

    /**
     * 根据规则id删除规则
     * @param ruleInfoId
     */
    void deleteOneRuleInfoById(Long ruleInfoId);
}
