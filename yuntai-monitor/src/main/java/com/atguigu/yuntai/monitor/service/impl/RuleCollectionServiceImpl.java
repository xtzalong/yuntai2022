package com.atguigu.yuntai.monitor.service.impl;

import com.atguigu.yuntai.monitor.entity.RuleCollection;
import com.atguigu.yuntai.monitor.mapper.TargetHiveTableMapper;
import com.atguigu.yuntai.monitor.service.RuleCollectionService;
import com.atguigu.yuntai.monitor.service.RuleInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 规则集合相关实现类
 **/
@Service
public class RuleCollectionServiceImpl extends ServiceImpl<TargetHiveTableMapper, RuleCollection> implements RuleCollectionService {

    @Autowired
    RuleInfoService ruleInfoService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeOneCollectionById(Long collectionId) {
        //删除规则集合
        this.removeById(collectionId);
        //删除规则集合下的所有规则
        ruleInfoService.deleteRuleInfoByCollectionId(collectionId);
    }

    @Override
    public void createOneCollection(RuleCollection ruleCollection) {
        this.save(ruleCollection);
    }

    @Override
    public List<RuleCollection> listCollectionsByHiveInfo(String database, String tableName) {
        return query().eq("hive_database", database).eq("hive_table_name", tableName).list();
    }
}
