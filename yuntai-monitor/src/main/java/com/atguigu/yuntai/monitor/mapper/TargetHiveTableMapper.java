package com.atguigu.yuntai.monitor.mapper;

import com.atguigu.yuntai.monitor.entity.RuleCollection;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 *
 *
 */
@DS("mysql-yuntai-monitor")
public interface TargetHiveTableMapper extends BaseMapper<RuleCollection> {
}
