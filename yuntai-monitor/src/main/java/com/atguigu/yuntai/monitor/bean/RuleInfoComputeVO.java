package com.atguigu.yuntai.monitor.bean;

import com.atguigu.yuntai.monitor.entity.AlertMsg;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * <p>除去规则的报警，可能还有一些是和触发有关的。</p>
 **/
@Data
@AllArgsConstructor
public class RuleInfoComputeVO {

    List<RuleInfoComputeRowVO> ruleInfoComputeRowVOList;

    List<AlertMsg> alertMsgList;
}
