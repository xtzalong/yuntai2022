package com.atguigu.yuntai.monitor.service;

import com.atguigu.yuntai.monitor.exception.HiveSqlException;

import java.util.Map;

public interface HiveService {
    Map<String, String> runSql(String sql) throws HiveSqlException;
}
