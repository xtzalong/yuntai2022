package com.atguigu.yuntai.monitor.bean

import groovy.time.TimeCategory

import java.text.SimpleDateFormat

/**
 * dsl中可用的函数
 * @author realdengziqi
 */
class BaseFunction {

    static String date(String format, closure){
        def result = null
        use(TimeCategory){
            result = closure()
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format)
        return dateFormat.format(result)
    }
}
