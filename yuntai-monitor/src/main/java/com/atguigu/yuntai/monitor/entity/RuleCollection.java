package com.atguigu.yuntai.monitor.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/*
 * <p>对应target_hive_table表</p>
 **/
@Data
public class RuleCollection {

    /**
     * 自增id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * hive数据库名
     */
    private String hiveDatabase;

    /**
     * hive表名
     */
    private String hiveTableName;

    /**
     * 规则集合名称
     */
    private String name;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 逻辑删除
     */
    private Boolean ifDelete;
}
