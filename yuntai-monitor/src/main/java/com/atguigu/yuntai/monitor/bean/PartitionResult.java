package com.atguigu.yuntai.monitor.bean;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>分区信息(和dsl相关)</p>
 **/
@Data
@Accessors(chain = true)
public class PartitionResult {

    /**
     * 字段的名称
     */
    private String name;

    /**
     * 字段的值
     */
    private String value;

    /**
     * 字段的类型
     */
    private String type;

    /**
     * 如果是与数据相关的类型，转为sql where条件的时候是不加引号的
     * @return
     */
    @Override
    public String toString() {
        String s = type.toLowerCase();
        switch (s){
            case "string":
            case "date":
            case "varchar":
            case "char":
                return  name + "='" + value + "'";
            default:
                return name + "=" + value;
        }
    }

}
