package com.atguigu.yuntai.monitor.service;

import lombok.SneakyThrows;
import org.apache.hadoop.hive.metastore.api.Table;

import java.util.List;

/**
 *
 */
public interface HiveMetaService {
    List<String> queryAllDataBases() ;

    @SneakyThrows
    List<String> queryAllTables(String database);

    @SneakyThrows
    Table queryDetailOfOneTable(String database, String tableName);
}
