package com.atguigu.yuntai.monitor.service.impl;

import com.atguigu.yuntai.monitor.exception.HiveSqlException;
import com.atguigu.yuntai.monitor.mapper.HiveSqlMapper;
import com.atguigu.yuntai.monitor.service.HiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>HiveService的实现类</p>
 *
 **/
@Service
public class HiveServiceImpl implements HiveService {

    @Autowired
    HiveSqlMapper hiveSqlMapper;

    /**
     * 运行一个生成的sql
     * @return
     */
    @Override
    public Map<String, String> runSql(String sql) throws HiveSqlException {
        Map<String, String> result;
        try {
            result = hiveSqlMapper.runSql(sql);
        }catch (Exception e) {
            throw new HiveSqlException(sql+"执行失败",e);
        }
        return result;
    }
}
