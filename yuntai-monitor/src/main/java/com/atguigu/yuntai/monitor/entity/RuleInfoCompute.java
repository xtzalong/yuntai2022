package com.atguigu.yuntai.monitor.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * <p>规则信息的复制</p>
 **/
@Data
@NoArgsConstructor
public class RuleInfoCompute extends RuleInfo {

    public RuleInfoCompute(Long triggerId,RuleInfo ruleInfo){
        BeanUtils.copyProperties(ruleInfo,this);
        this.id = null;
        this.ruleInfoId = ruleInfo.id;
        this.triggerId = triggerId;
    }

    /**
     * 自增id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * ruleInfo的id
     */
    private Long ruleInfoId;

    /**
     * 触发的id
     */
    private Long triggerId;

    /**
     * 基准分区的计算结果
     */
    private String baseResult;

    /**
     * 受检分区的计算结果
     */
    private String checkResult;

    /**
     * 是否触发了报警
     * @return
     */
    private Boolean ifAlert;


}
