package com.atguigu.yuntai.monitor.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * <p>hive的mapper</p>
 *
 **/
@DS("hive")
public interface HiveSqlMapper{

    /**
     * 执行一个中台生成的sql
     * @param sql
     * @return
     */
    @Select("${sql}")
    Map<String, String> runSql(@Param("sql") String sql);
}
