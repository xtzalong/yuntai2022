package com.atguigu.yuntai.monitor.entity;

import com.atguigu.yuntai.monitor.bean.PartitionDef;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;

import java.util.Date;

/**
 * <p>规则信息</p>
 **/
@Data
@TableName(value = "rule_info", autoResultMap = true)
public class RuleInfo {

    /**
     * 自增id
     */
    @TableId(value = "id",type = IdType.AUTO)
    public Long id;

    /**
     * 目标表的id(链表查询用)
     */
    private Long ruleCollectionId;

    /**
     * 规则的名称
     */
    private String name;

    /**
     * 分析的类型(数值分析，对比分析)
     */
    private String analyseType;

    /**
     * 规则级别(表级,字段级)
     */
    private String ruleLevel;

    /**
     * 基准分区表达式
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private PartitionDef basePartitionDef;

    /**
     * 受检分区表达式
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private PartitionDef checkPartitionDef;

    /**
     * 字段名，允许多个，使用逗号分隔
     */
    private String fieldsName;

    /**
     * 计算类型(总行数,增长率,控制率,命中率等)
     */
    private String calculateType;

    /**
     * 固定值的比较方式 > < >= <=等
     */
    private String fixedCompareWay;

    /**
     * 固定值
     */
    private String fixedValue;

    /**
     * 波动上限
     */
    private String swingUpper;

    /**
     * 波动下限
     */
    private String swingLower;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除
     */
    private Boolean ifDelete;
}
