package com.atguigu.yuntai.monitor.service.impl;

import com.atguigu.yuntai.monitor.bean.AlertMsgRowVO;
import com.atguigu.yuntai.monitor.bean.RuleInfoComputeRowVO;
import com.atguigu.yuntai.monitor.bean.RuleInfoComputeVO;
import com.atguigu.yuntai.monitor.entity.AlertMsg;
import com.atguigu.yuntai.monitor.entity.RuleInfoCompute;
import com.atguigu.yuntai.monitor.mapper.RuleInfoComputeMapper;
import com.atguigu.yuntai.monitor.service.AlertMsgService;
import com.atguigu.yuntai.monitor.service.RuleInfoComputeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>RuleInfoCompued的实现</p>
 *
 **/
@Service
public class RuleInfoComputeServiceImpl extends ServiceImpl<RuleInfoComputeMapper, RuleInfoCompute> implements RuleInfoComputeService {

    @Autowired
    AlertMsgService alertMsgService;

    @Override
    public void saveRuleInfoComputeList(List<RuleInfoCompute> ruleInfoComputeList) {
        saveOrUpdateBatch(ruleInfoComputeList);
    }

    @Override
    public RuleInfoComputeVO getRuleInfoComputeVOByTriggerId(Long triggerId) {
        // 0. 拿到所有的计算结果
        List<RuleInfoCompute> ruleInfoComputeList = query()
                .eq("trigger_id", triggerId)
                .list();

        // 1. 拿到所有的报警信息
        List<AlertMsgRowVO> alertMsgList = alertMsgService.listAlertMsgRowVoByTriggerId(triggerId);
        // 2. 将报警信息做成map
        Map<Object, List<AlertMsg>> alertMsgMap= alertMsgList.stream()
                .collect(
                        Collectors.toMap(
                                alertMsg -> {
                                    Long ruleInfoComputeId = alertMsg.getRuleInfoComputeId();
                                    if (ruleInfoComputeId == null) {
                                        return new Long(-1L);
                                    }
                                    return ruleInfoComputeId;
                                },
                                (AlertMsg alertMsg) -> {
                                    List<AlertMsg> l = new LinkedList<>();
                                    l.add(alertMsg);
                                    return l;
                                },
                                (List<AlertMsg> l1,List<AlertMsg> l2) -> {
                                    l1.addAll(l2);
                                    return l1;
                                }
                        )
                );
        // 3.遍历报警信息，进行join
        List<RuleInfoComputeRowVO> ruleInfoComputeRowVOList = ruleInfoComputeList.stream()
                .map(
                        ruleInfoCompute -> {
                            List<AlertMsg> list = alertMsgMap.get(ruleInfoCompute.getId());
                            return new RuleInfoComputeRowVO(ruleInfoCompute, list);
                        }
                ).collect(Collectors.toList());
        // 4. 索引为-1的不是规则级别的报警
        return new RuleInfoComputeVO(ruleInfoComputeRowVOList,alertMsgMap.get(new Long(-1L)));
    }
}
