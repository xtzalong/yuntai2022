package com.atguigu.yuntai.monitor.exception;

/**
 * <p>hivesql的执行异常</p>

 **/
public class HiveSqlException extends Exception {
    public HiveSqlException(String msg, Throwable e) {
        super(msg,e);
    }
}
