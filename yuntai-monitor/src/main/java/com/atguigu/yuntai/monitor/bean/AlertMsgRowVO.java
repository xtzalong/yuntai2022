package com.atguigu.yuntai.monitor.bean;

import com.atguigu.yuntai.monitor.entity.AlertMsg;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>前端页面上展示的报警信息的一行</p>
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlertMsgRowVO extends AlertMsg {

    /**
     * hive数据库
     */
    private String hiveDatabase;

    /**
     * hive表名
     */
    private String hiveTableName;

    /**
     * 规则集合的名称
     */
    private String collectionName;

    /**
     * 规则计算的名称(计算时，对应的那个规则的名称)
     */
    private String ruleComputeName;

    /**
     * 谁触发的
     */
    private String who;

}
