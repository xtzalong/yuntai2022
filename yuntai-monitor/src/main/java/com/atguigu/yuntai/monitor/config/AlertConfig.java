package com.atguigu.yuntai.monitor.config;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONObject;
import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>报警配置</p>
 **/
@Configuration
@ConfigurationProperties(prefix = "alert")
@Data
public class AlertConfig {

    String app;

    String url;

    @Bean
    @Qualifier("alertClient")
    public AlertClient alterClient() {
         return new AlertClient(app,url);
    }

    /**
     * 报警客户端
     */
    public static class AlertClient {

        String app;

        String url;

        public AlertClient(String app,String url) {
            this.app = app;
            this.url = url;
        }

        public Integer alert(String msg){
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("app",app);
            jsonObject.set("alarmName","离线数据质量");
            jsonObject.set("eventType","trigger");
            jsonObject.set("alarmContent",msg);
            HttpResponse execute = HttpRequest.post(url)
                    .body(jsonObject.toString())
                    .execute();
            System.out.println(execute.body());
            return execute.getStatus();
        }
    }
}
