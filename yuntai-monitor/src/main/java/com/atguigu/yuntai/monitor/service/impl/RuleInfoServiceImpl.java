package com.atguigu.yuntai.monitor.service.impl;

import com.atguigu.yuntai.monitor.entity.RuleInfo;
import com.atguigu.yuntai.monitor.mapper.RuleInfoMapper;
import com.atguigu.yuntai.monitor.service.RuleInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>RuleInfoService的实现类</p>
 *
 **/
@Service
public class RuleInfoServiceImpl extends ServiceImpl<RuleInfoMapper,RuleInfo> implements RuleInfoService {

    @Override
    public List<RuleInfo> listAllRulesByCollectionId(Long collectionId) {
        return query()
                .eq("rule_collection_id",collectionId)
                .eq("if_delete",0)
                .list();
    }

    @Override
    public void deleteRuleInfoByCollectionId(Long collectionId) {
        Map<String, Object> map = new HashMap<>(1);
        map.put("rule_collection_id",collectionId);
        removeByMap(map);
    }

    @Override
    public void saveOrUpdateOneRuleInfo(RuleInfo ruleInfo) {
        saveOrUpdate(ruleInfo);
    }

    @Override
    public RuleInfo getOneRuleInfoById(Long ruleInfoId) {
        return getById(ruleInfoId);
    }

    @Override
    public void deleteOneRuleInfoById(Long ruleInfoId) {
        removeById(ruleInfoId);
    }
}
