package com.atguigu.yuntai.monitor.mapper;

import com.atguigu.yuntai.monitor.bean.TriggerLogRowVO;
import com.atguigu.yuntai.monitor.entity.TriggerLog;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 *触发mapper
 */
@DS("mysql-yuntai-monitor")
public interface TriggerLogMapper extends BaseMapper<TriggerLog> {
    @Select("select t1.id,t1.collection_id,t1.who,t1.if_alert,t1.trigger_time,t1.statue,t1.computed_time,t2.hive_database,t2.hive_table_name,t2.name " +
            "from (select id,collection_id,who,if_alert,trigger_time,statue,computed_time" +
            "   from trigger_log" +
            "   order by id desc" +
            "  ) t1 " +
            "left join rule_collection t2 " +
            "on t1.collection_id = t2.id ")
    IPage<TriggerLogRowVO> listTriggerLogRowVO(Page<TriggerLogRowVO> page);
}
