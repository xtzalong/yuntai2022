package com.atguigu.yuntai.lineage.bean;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

@NodeEntity
@Data
@NoArgsConstructor
public class TableNode {

    @Id
    String id;

    @Property("name")
    String name ;

//    @Relationship(type="Insert",direction = Relationship.INCOMING)
//    List<Insert> insertList =new ArrayList<>();

    public TableNode( String tableName){
        this.id=tableName;
        this.name=tableName;
    }

    public TableNode( String id,String name){
        this.id=id;
        this.name=name;
    }

//    public void addSourceNode(TableNode sourceNode){
//        Insert insert  = new Insert(  sourceNode ,this);
//        insertList.add(insert);
//    }


    public TableNode withId(String id){
         if(id.equals(this.id)){
             return  this;
         }else{
             return new TableNode(  id,name );
         }
    }
}
