package com.atguigu.yuntai.lineage.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EchartLink {

    String source;

    String target;
}
