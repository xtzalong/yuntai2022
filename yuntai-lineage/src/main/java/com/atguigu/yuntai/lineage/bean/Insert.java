package com.atguigu.yuntai.lineage.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.*;
import org.springframework.data.annotation.Version;


@Data
@RelationshipEntity
@AllArgsConstructor
@NoArgsConstructor
public class Insert {



    @StartNode
    TableNode startNode;

    @EndNode
    TableNode endNode;


    @Id
    @GeneratedValue
    private     Long id ;

    @Version
    Long version;


    public Insert(String  startNodeStr,String endNodeStr){
        startNode = new TableNode( startNodeStr );
        endNode = new TableNode(endNodeStr);

    }


    public Insert(  TableNode  startNode, TableNode endNode ){
        this.startNode  =startNode;
        this.endNode  =endNode;

    }




}
