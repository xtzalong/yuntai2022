package com.atguigu.yuntai.lineage.bean;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class Node {

    int level=1;

    int x ;

    int y;

    String tableName;

    Set<Node> fromNodes=new HashSet<>();

    Set<Node> targetNodes=new HashSet<>();


    public void   addFromNode(Node node){

        if(!fromNodes.contains(node)){
            fromNodes.add(node);
        }
    }

    public void   addTargetNode(Node node){
        if(!targetNodes.contains(node)){
            targetNodes.add(node);
        }

    }

    @Override
    public String toString() {
        return "Node{" +
                "tableName='" + tableName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return tableName != null ? tableName.equals(node.tableName) : node.tableName == null;
    }

    @Override
    public int hashCode() {
        return tableName != null ? tableName.hashCode() : 0;
    }



}
